;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.project)


(def ^:private ^:dynamic *projects* (volatile! {}))
(def ^:private ^:dynamic *current-project* (volatile! nil))


;;;
;;; create
;;;

(defn create
   [name material-path world-units-per-meter max-world-coordinate valid-classes valid-properties]
   {:name name
    :material-path material-path
    :world-units-per-meter world-units-per-meter
    :max-world-coordinate max-world-coordinate
    :valid-classes (sorted-set valid-classes)
    :valid-properties (sorted-set valid-properties)})


;;;
;;; define-project
;;;

(defmacro define-project
   [name material-path world-units-per-meter max-world-coordinate valid-classes valid-properties]
   `(let [project# (create ~name ~material-path ~world-units-per-meter ~max-world-coordinate ~valid-classes ~valid-properties)]
         (vreset! *projects* (assoc @*projects* ~name project#))
         project#))


;;;
;;; use-project
;;;

(defn use-project
   [name]
   (vreset! *current-project* (@*projects* name)))


;material-path
;world-directory
;world-units-per-meter
;valid-classes = #{:world, :light, :liquid, :trigger, :model, :animation}
;valid-components = #{:world, :light, :liquid, :trigger, :model, :animation}
;valid-properties = #{}