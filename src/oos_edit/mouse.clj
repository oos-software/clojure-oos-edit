;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.mouse)


;;;
;;; create-state
;;;

(defn create-state
   []
   {:left-button    false
    :left-dragged   false
    :left-pos       [0.0 0.0]
    :middle-button  false
    :middle-dragged false
    :middle-pos     [0.0 0.0]
    :right-button   false
    :right-dragged  false
    :right-pos      [0.0 0.0]})


;;;
;;; event-button-name
;;;

(defn event-button-name
   [event]
   (case (.ordinal (.getButton event))
      1 :left-button
      2 :middle-button
      3 :right-button))


;;;
;;; event-dragged-name
;;;

(defn event-dragged-name
   [event]
   (case (.ordinal (.getButton event))
      1 :left-dragged
      2 :middle-dragged
      3 :right-dragged))


;;;
;;; event-pos-name
;;;

(defn event-pos-name
   [event]
   (case (.ordinal (.getButton event))
      1 :left-pos
      2 :middle-pos
      3 :right-pos))