;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.events
   (:require
      [oos-edit.defs :as defs]
      [oos-edit.editor :as editor]
      [oos-edit.editor-misc :as misc])
   (:import
      (javafx.scene.control Alert Alert$AlertType ButtonType)))


(defmulti handle-menu-event :event)


;;;
;;; editor
;;;

(defmethod handle-menu-event :editor/about
   [_]
   (let [alert (Alert. Alert$AlertType/INFORMATION)
         button (ButtonType. "Painly yours")]
      (.setTitle alert "About")
      (.setHeaderText alert (str defs/+oos-edit-name+ " (" defs/+oos-edit-version+ ")"))
      (.setContentText alert defs/+oos-edit-copyright+)
      (.clear (.getButtonTypes alert))
      (.add (.getButtonTypes alert) button)
      (.showAndWait alert)))


(defmethod handle-menu-event :editor/quit
   [_]
   (editor/quit))


(defmethod handle-menu-event :world/new
   [_]
   (editor/new-world))


(defmethod handle-menu-event :world/open
   [_]
   (editor/open-world))


;;;
;;; world
;;;

(defmethod handle-menu-event :world/save
   [_]
   (editor/save-world))


(defmethod handle-menu-event :world/save-as
   [_]
   (editor/save-world-as))


(defmethod handle-menu-event :export/oos-tech
   [_]
   (editor/export-world))


(defmethod handle-menu-event :world/rebuild-geometry
   [_]
   (editor/rebuild-geometry))


;;;
;;; brush
;;;

(defmethod handle-menu-event :brush/to-additive
   [_]
   (editor/handle-brush-function :to-additive))


(defmethod handle-menu-event :brush/to-subtractive
   [_]
   (editor/handle-brush-function :to-subtractive))


(defmethod handle-menu-event :brush/to-first
   [_]
   (editor/handle-brush-function :to-first))


(defmethod handle-menu-event :brush/to-last
   [_]
   (editor/handle-brush-function :to-last))


;;;
;;; geometry snapping
;;;

(defmethod handle-menu-event :geometry-snapping/one
   [_]
   (misc/set-geometry-snapping 1))


(defmethod handle-menu-event :geometry-snapping/two
   [_]
   (misc/set-geometry-snapping 2))


(defmethod handle-menu-event :geometry-snapping/four
   [_]
   (misc/set-geometry-snapping 4))


(defmethod handle-menu-event :geometry-snapping/eight
   [_]
   (misc/set-geometry-snapping 8))


(defmethod handle-menu-event :geometry-snapping/sixteen
   [_]
   (misc/set-geometry-snapping 16))


(defmethod handle-menu-event :geometry-snapping/thirty-two
   [_]
   (misc/set-geometry-snapping 32))


;;;
;;; rotation snapping
;;;

(defmethod handle-menu-event :rotation-snapping/one
   [_]
   (misc/set-rotation-snapping 1))


(defmethod handle-menu-event :rotation-snapping/fifteen
   [_]
   (misc/set-rotation-snapping 15))


(defmethod handle-menu-event :rotation-snapping/thirty
   [_]
   (misc/set-rotation-snapping 30))


(defmethod handle-menu-event :rotation-snapping/forty-five
   [_]
   (misc/set-rotation-snapping 45))


(defmethod handle-menu-event :rotation-snapping/ninety
   [_]
   (misc/set-rotation-snapping 90))


(defmethod handle-menu-event :default
   [e]
   (println "WARNING: unhandled menu event!" e))