;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.export.export
   (:require
      [clojure.pprint :as pp]
      [oos-edit.export.oos-tech :as oos-tech]))


;;; TODO: the menu entries for custom exporters must be handled!


(def ^:dynamic *registered-world-exporters* (volatile! {}))


;;;
;;; register-world-exporter
;;;

(defn- register-world-exporter
   [name callback]
   (vswap! *registered-world-exporters* assoc name callback))


;;;
;;; export-world
;;;

(defn export-world
   [exporter-name file-name world]
   (let [exporter (get @*registered-world-exporters* exporter-name)]
      (when exporter
         (let [prep (exporter world)
               out (with-out-str (pp/pprint prep))]
            (spit file-name out)))))


;;;
;;; all registered world exporters
;;;

(register-world-exporter :oos-tech oos-tech/prepare-world-for-export)