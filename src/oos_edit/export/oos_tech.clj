;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.export.oos-tech
   (:require
      [oos-edit.brush :as brush]
      [oos-edit.world :as world]
      [oos-edit.renderer.material :as material]
      [oos-edit.renderer.texture :as texture]))


;;;
;;; prepare-plane
;;;

(defn- prepare-plane
   [plane]
   (let [n (:normal plane)]
      {:a (:x n) :b (:y n) :c (:z n) :d (:dist plane)}))


;;;
;;; create-face
;;;

(defn- create-face
   [side plane points]
   (let [material (material/cache (:material side))
         width (material/width material)
         height (material/height material)
         tu (:translation-u side)
         tv (:translation-v side)
         su (:scale-u side)
         sv (:scale-v side)
         rot (:rotation side)
         mode (:mode side)
         tex-coord #(texture/calculate-texture-coordinates % (:normal side) width height tu tv su sv rot mode)]
      {:plane    plane,
       :material (:material side),
       :vertexes (into [] (map (fn [p]
                                  (let [[s t] (tex-coord p)]
                                     (assoc p :s s :t t))) points))}))


;;;
;;; prepare-brushes
;;;

(defn- prepare-brushes
   [brushes]
   (into [] (map (fn [[_ brush]]
                    (let [windings (brush/create-windings brush)
                          planes (into [] (map prepare-plane (brush/create-planes brush)))]
                       {:faces (into [] (map (fn [s p w] (create-face s p w)) (:sides brush) planes windings))})) brushes)))


;;;
;;; prepare-entity-for-export
;;;

(defn- prepare-entity-for-export
   [world [entity-id entity]]
   (let [brushes (if (= entity-id 0)
                    (into (sorted-map) (map-indexed vector (world/rebuild-geometry world)))
                    (:brushes entity))
         brushes (prepare-brushes brushes)]
      (assoc (dissoc entity :class :next-min-brush-id :next-max-brush-id :group) :brushes brushes)))


;;;
;;; prepare-world-for-export
;;;

(defn prepare-world-for-export
   [world]
   {:author   (:author world)
    :name     (:name world)
    :entities (into [] (map #(prepare-entity-for-export world %) (:entities world)))})



