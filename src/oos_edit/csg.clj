;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.csg
   (:require
      [oos-edit.math.plane :as plane]
      [oos-edit.math.quaternion :as quaternion]
      [oos-edit.position :as position]
      [oos-edit.brush :as brush]
      [oos-edit.math.vector :as vector]))


(def ^:private ^:const +epsilon+ 0.01)
(def ^:private ^:const +no-draw-material+ "engine/no-draw.png") ; TODO: is this project specific?


;;;
;;; count-sides
;;;

(defn- count-sides
   [plane winding]
   (let [distances (map #(plane/distance-from plane %) winding)
         num-front (count (filter #(> % +epsilon+) distances))
         num-back (count (filter #(< % +epsilon+) distances))
         num-on (- (count distances) (+ num-front num-back))]
      [num-front num-back num-on]))


;;;
;;; all-windings-in-front
;;;

(defn- all-windings-in-front
   [plane windings]
   (let [counts (map #(count-sides plane %) windings)
         num-front (count (filter (fn [[num-front num-back _]] (and (> num-front 0) (= num-back 0))) counts))]
      (= (count windings) num-front)))


;;;
;;; brush-overlapping
;;;

(defn- brush-overlapping
   [planes windings]
   (empty? (filter #(= % true) (map #(all-windings-in-front % windings) planes))))


;;;
;;; brushes-overlapping
;;;

(defn- brushes-overlapping
   [brush-planes brush-windings cutter-planes cutter-windings]
   (and (brush-overlapping brush-planes cutter-windings) (brush-overlapping cutter-planes brush-windings)))


;;;
;;; partition-sides
;;;

(defn- partition-sides
   [cutter sides planes windings]
   (let [counts (map vector (map #(count-sides cutter %) windings) sides planes)
         front-windings (filter (fn [[[num-front num-back _] _ _]] (and (> num-front 0) (= num-back 0))) counts)
         back-windings (filter (fn [[[num-front num-back _] _ _]] (and (= num-front 0) (> num-back 0))) counts)
         both-windings (filter (fn [[[num-front num-back _] _ _]] (and (> num-front 0) (> num-back 0))) counts)
         extract (fn [collection] (map (fn [[_ side plane]] [side plane]) collection))]
      [(extract front-windings) (extract back-windings) (extract both-windings)]))


;;;
;;; create-brush
;;;

(defn- create-brush
   [cutter brush sides-planes additional invert]
   (let [origin (position/add (:origin cutter) (:offset additional))
         offset (position/subtract origin (:origin brush))
         sides (map #(first %) sides-planes)
         additional (assoc additional :offset offset)
         additional (if invert (assoc additional :normal (vector/inverse (:normal additional)))
                               (assoc additional :material +no-draw-material+))]
      (brush/create (:origin brush) quaternion/+unit+ (conj sides additional) :additive)))


;;;
;;; split-brush
;;;

(defn- split-brush
   [cutter-brush cutter-side cutter-plane brush sides planes windings]
   (let [[front back both] (partition-sides cutter-plane sides planes windings)]
      [(create-brush cutter-brush brush (concat front both) cutter-side true)
       (create-brush cutter-brush brush (concat back both) cutter-side false)]))


;;;
;;; cut-brush
;;;
;;; the back result (if some) needs to be processed further
;;; only brushes with four or more planes are valid
;;;

(defn- cut-brush
   [cutter-brush cutter-sides cutter-planes brush]
   (loop [index 0
          to-cut brush
          result []]
      (if (< index (count cutter-sides))
         (let [sides (:sides to-cut)
               planes (brush/create-planes to-cut)
               windings (brush/create-windings to-cut)
               [front back] (split-brush cutter-brush (nth cutter-sides index) (nth cutter-planes index) to-cut sides planes windings)
               new-result (if (< (count (:sides front)) 4) result (conj result front))]
            (if (< (count (:sides back)) 4)
               new-result
               (recur (+ index 1)
                      back
                      new-result)))
         result)))


;;;
;;; perform-subtraction
;;;

(defn perform-subtraction
   [cutter-brush brushes]
   (let [brush-planes (map brush/create-planes brushes)
         brush-windings (map brush/create-windings brushes)
         cutter-sides (:sides cutter-brush)
         cutter-planes (brush/create-planes cutter-brush)
         cutter-windings (brush/create-windings cutter-brush)
         overlapping (map (fn [planes windings] (brushes-overlapping planes windings cutter-planes cutter-windings)) brush-planes brush-windings)]
      (loop [index 0
             result []]
         (if (< index (count overlapping))
            (let [brush (nth brushes index)]
               (recur (+ index 1)
                      (concat result (if-not (nth overlapping index)
                                        [brush]
                                        (cut-brush cutter-brush cutter-sides cutter-planes brush)))))
            result))))