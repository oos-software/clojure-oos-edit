;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.menu.orthographic-menu
   (:import
      (javafx.scene.control Menu MenuItem ContextMenu)
      (javafx.event EventHandler)))


; TODO: create automatically based on entity-definitions!

;(def ^:dynamic *orthographic-menu* (volatile! nil))


(defn- event-handler
   [handler event]
   (reify EventHandler
      (handle [_ _]
         (handler event))))


(defn- create-menu-item
   [handler text event]
   (let [item (MenuItem. text)]
      (.setOnAction item (event-handler handler event))
      item))


(defn- create-menu
   [text items]
   (let [menu (Menu. text)]
      (doseq [x items] (.add (.getItems menu) x))
      menu))


(defn- create-context-menu
   [items]
   (let [menu (ContextMenu.)]
      (doseq [x items] (.add (.getItems menu) x))
      menu))


(defn- create-info-menu
   [handler]
   (create-menu "info" [(create-menu-item handler "info-player-start" :info-player-start)]))


(defn- create-light-menu
   [handler]
   (create-menu "light" [(create-menu-item handler "light-sky" :light-sky)
                         (create-menu-item handler "light-point" :light-point)]))


(defn create-orthographic-menu
   [handler]
   (create-context-menu [(create-info-menu handler)
                         (create-light-menu handler)
                         (create-menu-item handler "Edit Entity" :edit-entity)]))  ; TODO!