;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.world
   (:require
      [oos-edit.entity :as entity]
      [oos-edit.csg :as csg]))


(def ^:private ^:const +world-units-per-meter+ 64.0)  ; TODO: this should be a property of the project?


;;;
;;; metric->world
;;;

(defn metric->world
   [^double metric]
   (* metric +world-units-per-meter+))


;;;
;;; world->metric
;;;

(defn world->metric
   [^double world]
   (/ world +world-units-per-meter+))


;;;
;;; def-world-changer
;;;

(defmacro def-world-changer
   [key]
   `(defn- ~(symbol (str "change-world-" (name key)))
       [world# value#]
       (assoc world# ~key value#)))


(def-world-changer :author)
(def-world-changer :name)
(def-world-changer :properties)
(def-world-changer :entities)


;;;
;;; get-entity
;;;

(defn get-entity
   [world ^long id]
   (get (:entities world) id))


;;;
;;; add-entity
;;;

(defn add-entity
   [world entity]
   (let [id (:next-entity-id world)
         new-world (assoc world :next-entity-id (inc id))
         new-entities (assoc (:entities world) id entity)]
      (change-world-entities new-world new-entities)))


;;;
;;; remove-entity
;;;

(defn remove-entity
   [world ^long id]
   (let [new-entities (dissoc (:entities world) id)]
      (change-world-entities world new-entities)))


;;;
;;; update-entity
;;;

(defn update-entity
   [world ^long id entity]
   (let [new-entities (assoc (:entities world) id entity)]
      (change-world-entities world new-entities)))


;;;
;;; rebuild-geometry-internal
;;;

(defn- rebuild-geometry-internal
   [brushes cutters]
   (loop [index 0
          to-cut brushes]
      (if (< index (count cutters))
         (let [result (csg/perform-subtraction (nth cutters index) to-cut)]
            (recur (+ index 1)
                   result))
         to-cut)))


;;;
;;; rebuild-geometry-recursive
;;;

(defn- rebuild-geometry-recursive
   [result brushes]
   (let [to-cut (concat result (first brushes))
         cutters (second brushes)]
      (if (not (some? cutters))
         to-cut
         (let [temp-result (rebuild-geometry-internal to-cut cutters)]
            (rebuild-geometry-recursive temp-result (drop 2 brushes))))))


;;;
;;; rebuild-geometry
;;;

(defn rebuild-geometry
   [world]
   (let [world-entity (get-entity world 0)
         flat-brushes (into (vector) (vals (:brushes world-entity)))
         brushes (partition-by #(= (:mode %) :subtractive) flat-brushes)]
      (if (<= (count brushes) 1)
         (first brushes)
         (rebuild-geometry-recursive (vector) brushes))))


;;;
;;; process-after-loading
;;;

(defn process-after-loading
   [world]
   (assoc world
      :properties (into (sorted-map) (:properties world))
      :entities (into (sorted-map) (map (fn [[key entity]] [key (entity/process-after-loading entity)]) (:entities world)))))


;;;
;;; create
;;;

(defn create
   []
   (let [world {:author         "UNKNOWN"
                :name           "UNNAMED"
                :next-entity-id (long 0)                    ; always increasing, never reused
                :properties     (sorted-map)
                :entities       (sorted-map)}]
      ;; we've always a world and it's always entity 0
      (add-entity world (entity/create :world))))