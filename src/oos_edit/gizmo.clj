;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.gizmo
   (:import (javafx.scene.shape Cylinder)))


(def ^:const +cylinder-height+ 128.0)
(def ^:const +cylinder-radius+ 4.0)


;;;
;;; setup-cylinder
;;;

(defn- setup-cylinder
   [cylinder]
   (.setHeight cylinder +cylinder-height+)
   (.setRadius cylinder +cylinder-radius+))


;;;
;;; create-translate-gizmo
;;;

(defn create-translate-gizmo
   []
   (let [x-axis (Cylinder.)
         y-axis (Cylinder.)
         z-axis (Cylinder.)]
      (setup-cylinder x-axis)
      (setup-cylinder y-axis)
      (setup-cylinder z-axis)
      {:x x-axis :y y-axis :z z-axis}))


;;;
;;; create-rotate-gizmo
;;;

(defn create-rotate-gizmo
   []

   )


;;;
;;; create-scale-gizmo
;;;

(defn create-scale-gizmo
   []

   )