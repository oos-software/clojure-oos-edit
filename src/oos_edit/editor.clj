;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.editor
   (:require
      [clojure.edn :as edn]
      [clojure.pprint :as pp]
      [oos-edit.control.tab-pane :as tab-pane]
      [oos-edit.control.world-area :as world-area]
      [oos-edit.control.scene-utils :as scene-utils]
      [oos-edit.renderer.render-data :as render-data]
      [oos-edit.dialog.world :as world-dialog]
      [oos-edit.keyboard :as keyboard]
      [oos-edit.view :as view]
      [oos-edit.world :as world]
      [oos-edit.brush :as brush]
      [oos-edit.undo :as undo]
      [oos-edit.export.export :as export])
   (:import
      (com.sun.javafx.scene.control CustomColorDialog)
      (java.io File)
      (javafx.animation AnimationTimer)
      (javafx.stage FileChooser FileChooser$ExtensionFilter)
      (javafx.scene Group)))


; TODO: persist/restore view origins/rotations for the world
; TODO: set tab content deferred and use tab as parameter


(def ^:private ^:dynamic *root-stage* (volatile! nil))
(def ^:private ^:dynamic *keyboard-state* (volatile! nil))
(def ^:private ^:dynamic *last-time-stamp* (volatile! nil))


;;;
;;; create-tab-state
;;;

(defn- create-tab-state
   [file-name world]
   {:file-name                file-name
    :modified                 false
    :world                    world
    :side-view                (view/create :side)
    :top-view                 (view/create :top)
    :front-view               (view/create :front)
    :perspective-view         (view/create :perspective)
    :current-view             :top-view
    ;:translate-gizmo
    ;:rotate-gizmo
    ;:scale-gizmo
    :brush-meshes             {}
    :built-brushes            (list)
    :built-brushes-meshes     {}
    ;:orthographic-group       (Group.)
    ;:orthographic-built-group (Group.)
    ;:orthographic-temp-group  (Group.)
    ;:perspective-group        (Group.)
    ;:perspective-built-group  (Group.)
    ;:perspective-temp-group   (Group.)
    :orthographic-render-data (render-data/create)
    :perspective-render-data  (render-data/create)
    :undo-state               (undo/create-state)
    :picking-result           nil})


;;;
;;; on-animation-timer
;;;

(defn- on-animation-timer
   [keyboard-state time-stamp]
   (let [last @*last-time-stamp*
         delta-seconds (if (some? last) (/ (- time-stamp last) 1000000000.0) 0.0)]
      (world-area/handle-keyboard keyboard-state delta-seconds))
   (vreset! *last-time-stamp* time-stamp))


;;;
;;; animation-timer
;;;

(defn- animation-timer
   [keyboard-state]
   (proxy [AnimationTimer] []
      (handle ([time-stamp]
               (on-animation-timer keyboard-state time-stamp)))))


;;;
;;; show-file-chooser-with-title
;;;

(defn- show-file-chooser-with-title
   [title is-save]
   (let [chooser (FileChooser.)]
      (.setInitialDirectory chooser (File. "D:/projects/oos-edit/")) ; TODO: move to project!
      (.addAll (.getExtensionFilters chooser)
               [(FileChooser$ExtensionFilter. "Worlds" (into-array String ["*.edn"]))
                (FileChooser$ExtensionFilter. "All Files" (into-array String ["*.*"]))])
      (if is-save
         (do (.setTitle chooser title)
             (.showSaveDialog chooser @*root-stage*))
         (do (.setTitle chooser title)
             (.showOpenDialog chooser @*root-stage*)))))


;;;
;;; show-file-chooser
;;;

(defn- show-file-chooser
   [is-save]
   (show-file-chooser-with-title (if is-save "Save World" "Open World") is-save))


;;;
;;; show-color-picker
;;;

(defn- show-color-picker
   []
   (let [picker (CustomColorDialog. @*root-stage*)]
      ; TODO: get and use color
      (.show picker)))


;;;
;;; new-world-internal
;;;

(defn- new-world-internal
   [file-name tab-name world]
   (let [state (atom (create-tab-state file-name world))
         content (world-area/create-world-area @*root-stage* *keyboard-state* state)
         tab (tab-pane/add-tab tab-name state content)]     ; this is ugly, but the tab must be created before adding the drawing data
      (reduce-kv (fn [_ entity-id entity]
                    (reduce-kv (fn [_ brush-id brush]
                                  (let [[orthographic perspective] (scene-utils/add-brush-render-data-to-state state entity-id brush-id brush)]
                                     (scene-utils/add-render-data-to-tab tab orthographic perspective))) {} (:brushes entity))) {} (:entities world))
      tab))


;;;
;;; new-world
;;;

(defn new-world
   []
   ;(world-dialog/show-new-dialog)
   (new-world-internal nil "UNNAMED" (world/create)))


;;;
;;; open-world
;;;

(defn open-world
   []
   (let [file (show-file-chooser false)]
      (when (some? file)
         (let [file-name (.getAbsolutePath file)
               world (world/process-after-loading (edn/read-string (slurp file-name)))]
            (new-world-internal file-name file-name world)))))


;;;
;;; save-world-interval
;;;

(defn- save-world-internal
   [state ^String file-name]
   (spit file-name (with-out-str (pp/write (:world @state) :dispatch pp/code-dispatch)))
   (reset! state (assoc @state :file-name file-name :modified false)))


;;;
;;; save-world-as
;;;

(defn save-world-as
   []
   (tab-pane/with-current-tab [_ state]
                              (let [file (show-file-chooser true)]
                                 (when (some? file)
                                    (save-world-internal state (.getAbsolutePath file))))))


;;;
;;; save-world
;;;

(defn save-world
   []
   (tab-pane/with-current-tab [_ state]
                              (let [file-name (:file-name @state)]
                                 (if (nil? file-name)
                                    (save-world-as)
                                    (save-world-internal state file-name)))))


;;;
;;; save-open-worlds
;;;

(defn save-open-worlds                                      ; pun intended
   []
   (tab-pane/for-all-tabs (fn [tab state]
                             )))


;;;
;;; export-world
;;;

(defn export-world
   []
   (tab-pane/with-current-tab [_ state]
                              (let [file (show-file-chooser-with-title "Export World" true)]
                                 (when (some? file)
                                    (export/export-world :oos-tech (.getAbsolutePath file) (:world @state))))))


;;;
;;; rebuild-geometry
;;;

(defn rebuild-geometry
   []
   (tab-pane/with-current-tab [_ state]
                              (let [brushes (world/rebuild-geometry (:world @state))]
                                 (reset! state (assoc @state :built-brushes brushes)))))


;;;
;;; handle-brush-function
;;;

(defn handle-brush-function
   [function]
   (world-area/handle-brush-function function))


;;;
;;; quit
;;;

(defn quit
   []
   (.close @*root-stage*))


;;;
;;; initialize
;;;

(defn initialize
   [root-stage keyboard-state]
   (vreset! *root-stage* root-stage)
   (vreset! *keyboard-state* keyboard-state)
   (new-world)                                              ; TODO!
   (.start (animation-timer keyboard-state)))