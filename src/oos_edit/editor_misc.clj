;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.editor-misc
   (:require
      [oos-edit.position :as position]))


(def ^:private ^:dynamic *geometry-snapping* (volatile! 16))
(def ^:private ^:dynamic *rotation-snapping* (volatile! 15))


;;;
;;; get-geometry-snapping
;;;

(defn get-geometry-snapping
   [raw]
   (if raw
      @*geometry-snapping*
      (position/integer->fixed @*geometry-snapping*)))


;;;
;;; set-geometry-snapping
;;;

(defn set-geometry-snapping
   [snapping]
   (vreset! *geometry-snapping* snapping))


;;;
;;; get-rotation-snapping
;;;

(defn get-rotation-snapping
   []
   @*rotation-snapping*)


;;;
;;; set-rotation-snapping
;;;

(defn set-rotation-snapping
   [snapping]
   (vreset! *rotation-snapping* snapping))