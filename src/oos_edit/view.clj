;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.view
   (:require
      [oos-edit.math.vector :as vector]
      [oos-edit.math.utils :as utils])
   (:import
      (javafx.geometry Point3D)
      (javafx.scene ParallelCamera PerspectiveCamera)
      (javafx.scene.transform Rotate Translate Scale)))


(def ^:private ^:const +near-clip+ 5.0)
(def ^:private ^:const +far-clip+ 10000.0)
(def ^:private ^:const +vertical-fov+ 50.0)
(def ^:private ^:const +initial-perspective-origin+ (vector/->Vector3 0.0 0.0 256.0))

(def ^:private ^:const +max-world-coordinate+ 262144.0)     ; TODO: move to another place


;;;
;;; normalized-device-coordinates
;;;

(defn- normalized-device-coordinates
   [mouse-x mouse-y width height]
   (let [aspect (/ width height)
         rad (utils/degrees->radians (* +vertical-fov+ 0.5))
         vs (Math/tan rad)
         hs (* vs aspect)]
      [(* mouse-x hs)
       (* mouse-y vs)]))


;;;
;;; calculate-vectors
;;;

(defn calculate-vectors
   [rotation]
   (let [rotate-x (Rotate.)
         rotate-y (Rotate.)
         rotate-z (Rotate.)]
      ;; rotation
      (.setAxis rotate-x (Point3D. 1.0 0.0 0.0))
      (.setAngle rotate-x (vector/get-component rotation 0))
      (.setAxis rotate-y (Point3D. 0.0 1.0 0.0))
      (.setAngle rotate-y (vector/get-component rotation 1))
      (.setAxis rotate-z (Point3D. 0.0 0.0 1.0))
      (.setAngle rotate-z (vector/get-component rotation 2))
      ;; concat the transforms and calculate the vectors
      (let [rotate (.createConcatenation (.createConcatenation rotate-z rotate-y) rotate-x)]
         (let [right (.transform rotate 1.0 0.0 0.0)
               forward (.transform rotate 0.0 1.0 0.0)
               up (.transform rotate 0.0 0.0 1.0)]
            [(vector/->Vector3 (.getX right) (.getY right) (.getZ right))
             (vector/->Vector3 (.getX forward) (.getY forward) (.getZ forward))
             (vector/->Vector3 (.getX up) (.getY up) (.getZ up))]))))


;;;
;;; build-transformation
;;;

(defn build-transformation
   [origin rotation]
   (let [translate (Translate.)
         initial-rotate (Rotate.)
         rotate-x (Rotate.)
         rotate-y (Rotate.)
         rotate-z (Rotate.)]
      ;; put z going up
      (.setAxis initial-rotate (Point3D. 1.0 0.0 0.0))
      (.setAngle initial-rotate -90.0)
      ;; translation
      (.setX translate (vector/get-component origin 0))
      (.setY translate (vector/get-component origin 1))
      (.setZ translate (vector/get-component origin 2))
      ;; rotation
      (.setAxis rotate-x (Point3D. 1.0 0.0 0.0))
      (.setAngle rotate-x (vector/get-component rotation 0))
      (.setAxis rotate-y (Point3D. 0.0 1.0 0.0))
      (.setAngle rotate-y (vector/get-component rotation 1))
      (.setAxis rotate-z (Point3D. 0.0 0.0 1.0))
      (.setAngle rotate-z (vector/get-component rotation 2))
      [translate rotate-z rotate-y rotate-x initial-rotate]))


;;;
;;; initial-orthographic-rotation
;;;

(defn initial-orthographic-rotation
   [view]
   (let [rotate (Rotate.)
         mode (:mode view)
         axis (case mode
                 :side (Point3D. 0.0 0.0 1.0)
                 :front (Point3D. 0.0 1.0 0.0)
                 :top (Point3D. 1.0 0.0 0.0))
         angle (case mode
                  :side 90.0
                  :front 0.0
                  :top -90.0)]
      (.setAxis rotate axis)
      (.setAngle rotate angle)
      rotate))


;;;
;;; initial-orthographic-scale
;;;

(defn initial-orthographic-scale
   [view]
   (let [scale (Scale. 1.0 1.0 1.0)]
      ;(when (= (:mode view) :front)
      ;   (.setZ scale -1.0))
      scale))


;;;
;;; create-orthographic-camera
;;;

(defn create-orthographic-camera
   []
   (let [coord (+ +max-world-coordinate+ 0.5)]
      (doto (ParallelCamera.)
         (.setNearClip (- coord))
         (.setFarClip coord))))


;;;
;;; create-perspective-camera
;;;

(defn create-perspective-camera
   []
   (doto (PerspectiveCamera. true)
      (.setNearClip +near-clip+)
      (.setFarClip +far-clip+)
      (.setFieldOfView +vertical-fov+)
      (.setVerticalFieldOfView true)))


;;;
;;; create
;;;

(defn create
   "creates a view. the mode can be either :side, :front, :top or :perspective"
   [mode]
   {:mode     mode
    :origin   (if (= mode :perspective) +initial-perspective-origin+ vector/+zero+)
    :rotation vector/+zero+})


;;;
;;; display-name
;;;

(defn display-name
   [view]
   (let [mode (:mode view)
         name (clojure.string/capitalize (clojure.core/name mode))]
      (str name (case mode
                   :side " (YZ)"
                   :front " (XZ)"
                   :top " (XY)"))))


;;;
;;; offset
;;;

(defn offset
   [center-x center-y]
   (doto (Translate.)
      (.setX (- center-x))
      (.setY (- center-y))))


;;;
;;; transform-mouse
;;;

(defn transform-mouse
   [view mouse-x mouse-y]
   (let [origin (:origin view)
         x (vector/get-component origin 0)
         z (vector/get-component origin 2)]
      [(+ x mouse-x) (+ z mouse-y)]))


;;;
;;; translate
;;;

(defn translate
   [view x y]
   (let [move (vector/->Vector3 (- x) 0.0 (- y))
         new-origin (vector/round (vector/add (:origin view) move))]
      (assoc view :origin new-origin)))


;;;
;;; used-coordinates
;;;

(defn used-coordinates
   [view]
   (case (:mode view)
      :side [:y :z]
      :front [:x :z]
      :top [:x :y]))


;;;
;;; unused-coordinate
;;;

(defn unused-coordinate
   [view]
   (case (:mode view)
      :side :x
      :front :y
      :top :z))


;;;
;;; main-axes
;;;

(defn main-axes
   [view]
   (case (:mode view)
      :side [(vector/->Vector3 0.0 1.0 0.0) (vector/->Vector3 0.0 0.0 1.0)]
      :front [(vector/->Vector3 1.0 0.0 0.0) (vector/->Vector3 0.0 0.0 1.0)]
      :top [(vector/->Vector3 1.0 0.0 0.0) (vector/->Vector3 0.0 1.0 0.0)]))


;;;
;;; picking-ray
;;;

(defn picking-ray
   [view mouse-x mouse-y]
   (let [coord (+ +max-world-coordinate+ 0.5)
         [world-x world-y] (transform-mouse view mouse-x mouse-y)
         origin (case (:mode view)
                   :side (vector/->Vector3 coord world-x world-y)
                   :front (vector/->Vector3 world-x (- coord) world-y)
                   :top (vector/->Vector3 world-x world-y coord))
         direction (case (:mode view)
                      :side (vector/->Vector3 -1.0 0.0 0.0)
                      :front (vector/->Vector3 0.0 1.0 0.0)
                      :top (vector/->Vector3 0.0 0.0 -1.0))]
      [origin direction]))


;;;
;;; perspective-picking-ray
;;;

(defn perspective-picking-ray
   [view mouse-x mouse-y width height]
   (let [[nx ny] (normalized-device-coordinates mouse-x mouse-y width height)
         ray (vector/normalize (vector/->Vector3 nx 1.0 ny))
         [right forward up] (calculate-vectors (:rotation view))
         trans-right (vector/->Vector3 (:x right) (:x forward) (:x up))
         trans-forward (vector/->Vector3 (:y right) (:y forward) (:y up))
         trans-up (vector/->Vector3 (:z right) (:z forward) (:z up))
         x (vector/scalar-product ray trans-right)
         y (vector/scalar-product ray trans-forward)
         z (vector/scalar-product ray trans-up)]
      [(:origin view)
       (vector/->Vector3 x y z)]))