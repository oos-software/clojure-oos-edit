;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.picking
   (:require
      [oos-edit.math.vector :as vector]
      [oos-edit.entity :as entity]
      [oos-edit.brush :as brush]
      [oos-edit.world :as world]))


; TODO: support other entities than the world


;;;
;;; create-single-result
;;;

(defn- create-single-result
   [hit-point entity-id brush-id side-index]
   {:hit-point  hit-point
    :entity-id  entity-id
    :brush-id   brush-id
    :brush-side side-index})


;;;
;;; create-result
;;;

(defn create-result
   [results]
   {:index   0
    :results results})


;;;
;;; current-result
;;;

(defn current-result
   [result]
   (nth (:results result) (:index result)))


;;;
;;; prev-result
;;;

(defn prev-result
   [result]
   (let [size (count (:results result))
         new-index (mod (+ (:index result) (- size 1)) size)]
      (assoc result :index new-index)))


;;;
;;; next-result
;;;

(defn next-result
   [result]
   (let [size (count (:results result))
         new-index (mod (+ (:index result) size) size)]
      (assoc result :index new-index)))


;;;
;;; remove-result
;;;

(defn remove-result
   [result]
   (let [index (:index result)
         new-results (keep-indexed #(if (not= %1 index) %2) (:results result))]
      (if (empty? new-results)
         nil
         (assoc result :index 0))))


;;;
;;; with-selected-brush
;;;

(defn with-selected-brush
   [world result callback]
   (when (some? result)
      (let [result (current-result result)
            entity-id (:entity-id result)
            brush-id (:brush-id result)
            brush (entity/get-brush (world/get-entity world entity-id) brush-id)]
         (callback entity-id brush-id brush))))


;;;
;;; intersect-brushes
;;;

(defn- intersect-brushes
   [world ray-origin ray-direction]
   (let [world-entity (world/get-entity world 0)
         temp-results (map (fn [[id brush]] [id (brush/intersect-ray brush ray-origin ray-direction)]) (:brushes world-entity))
         filtered-results (filter (fn [[_ dist]] (>= dist 0.0)) temp-results)
         sorted-results (sort-by second filtered-results)
         results (map (fn [[id dist]]
                         (let [hit-point (vector/add ray-origin (vector/scale ray-direction dist))]
                            (create-single-result hit-point 0 id 0))) sorted-results)]
      (take 10 results)))


;;;
;;; pick-brushes
;;;

(defn pick-brushes
   [world ray-origin ray-direction]
   (let [results (intersect-brushes world ray-origin ray-direction)]
      (if (empty? results)
         nil
         (create-result results))))


;;;
;;; peek-brush
;;;

(defn peek-brush
   [world ray-origin ray-direction]
   (let [result (pick-brushes world ray-origin ray-direction)]
      (when (some? result)
         (current-result result))))


;;;
;;; pick-entities
;;;

(defn pick-entities
   [world ray-origin ray-direction]
   ; TODO: bounding-box, model etc.
   nil)


;;;
;;; peek-entity
;;;

(defn peek-entity
   [world ray-origin ray-direction]
   (let [result (pick-entities world ray-origin ray-direction)]
      (when (some? result)
         (current-result result))))