;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.renderer.render-data)


;;;
;;; create
;;;

(defn create
   []
   {:temp-render-data   {}
    :brush-render-data  {}
    :model-render-data  {}
    :sprite-render-data {}})


;;;
;;; add-render-data
;;;

(defn- add-render-data
   [state render-data-type render-data-key orthographic-render-data perspective-render-data]
   (let [temp-orthographic-render-data (assoc (get (:orthographic-render-data @state) render-data-type) render-data-key orthographic-render-data)
         temp-perspective-render-data (assoc (get (:perspective-render-data @state) render-data-type) render-data-key perspective-render-data)
         new-orthographic-render-data (assoc (:orthographic-render-data @state) render-data-type temp-orthographic-render-data)
         new-perspective-render-data (assoc (:perspective-render-data @state) render-data-type temp-perspective-render-data)]
      [new-orthographic-render-data new-perspective-render-data]))


;;;
;;; remove-render-data
;;;

(defn- remove-render-data
   [state render-data-type render-data-key]
   (let [orthographic-render-data (get (get (:orthographic-render-data @state) render-data-type) render-data-key)
         perspective-render-data (get (get (:perspective-render-data @state) render-data-type) render-data-key)
         temp-orthographic-render-data (dissoc (get (:orthographic-render-data @state) render-data-type) render-data-key)
         temp-perspective-render-data (dissoc (get (:perspective-render-data @state) render-data-type) render-data-key)
         new-orthographic-render-data (assoc (:orthographic-render-data @state) render-data-type temp-orthographic-render-data)
         new-perspective-render-data (assoc (:perspective-render-data @state) render-data-type temp-perspective-render-data)]
      [orthographic-render-data perspective-render-data new-orthographic-render-data new-perspective-render-data]))


;;;
;;; add-temp-render-data
;;;

(defn add-temp-render-data
   [state render-data-key orthographic-render-data perspective-render-data]
   (add-render-data state :temp-render-data render-data-key orthographic-render-data perspective-render-data))


;;;
;;; remove-temp-render-data
;;;

(defn remove-temp-render-data
   [state render-data-key]
   (remove-render-data state :temp-render-data render-data-key))


;;;
;;; add-brush-render-data
;;;

(defn add-brush-render-data
   [state render-data-key orthographic-render-data perspective-render-data]
   (add-render-data state :brush-render-data render-data-key orthographic-render-data perspective-render-data))


;;;
;;; remove-brush-render-data
;;;

(defn remove-brush-render-data
   [state render-data-key]
   (remove-render-data state :brush-render-data render-data-key))


;;;
;;; add-model-render-data
;;;

(defn add-model-render-data
   [state render-data-key orthographic-render-data perspective-render-data]
   (add-render-data state :model-render-data render-data-key orthographic-render-data perspective-render-data))


;;;
;;; remove-model-render-data
;;;

(defn remove-model-render-data
   [state render-data-key]
   (remove-render-data state :model-render-data render-data-key))


;;;
;;; add-sprite-render-data
;;;

(defn add-sprite-render-data
   [state render-data-key orthographic-render-data perspective-render-data]
   (add-render-data state :sprite-render-data render-data-key orthographic-render-data perspective-render-data))


;;;
;;; remove-sprite-render-data
;;;

(defn remove-sprite-render-data
   [state render-data-key]
   (remove-render-data state :sprite-render-data render-data-key))