;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.renderer.texture
   (:require
      [oos-edit.math.utils :as utils]
      [oos-edit.math.vector :as vector]))


;;; TODO: handle world/face texture coordinates


(defonce ^:private ^:const +texture-axes+
         [(vector/->Vector3 0.0 -1.0 0.0) (vector/->Vector3 0.0 0.0 1.0) ; -x
          (vector/->Vector3 0.0 1.0 0.0) (vector/->Vector3 0.0 0.0 1.0) ; x
          (vector/->Vector3 1.0 0.0 0.0) (vector/->Vector3 0.0 0.0 1.0) ; -y
          (vector/->Vector3 -1.0 0.0 0.0) (vector/->Vector3 0.0 0.0 1.0) ; y
          (vector/->Vector3 1.0 0.0 0.0) (vector/->Vector3 0.0 -1.0 0.0) ; -z
          (vector/->Vector3 1.0 0.0 0.0) (vector/->Vector3 0.0 1.0 0.0)]) ; z


;;;
;;; get-texture-axes
;;;

(defn- get-texture-axes
   [vertex]
   (let [i (vector/largest-index vertex)
         value (vector/get-component vertex i)
         index (+ (bit-shift-left i 2) (if (< value 0.0)
                                          0
                                          2))]
      [(+texture-axes+ index) (+texture-axes+ (inc index))]))


;;;
;;; calculate-texture-coordinates
;;;
;;; mode can be eiter :world (aligned to world axes) or :face (aligned to the face)
;;;

(defn calculate-texture-coordinates
   [vertex normal width height translation-u translation-v scale-u scale-v rotation mode]
   (let [a (utils/degrees->radians rotation)
         sin (Math/sin a)
         cos (Math/cos a)
         [right up] (get-texture-axes normal)
         u (vector/scalar-product right vertex)
         v (vector/scalar-product up vertex)
         su (* width scale-u)
         sv (* height scale-v)
         s (/ (+ u translation-u) su)
         t (/ (+ v translation-v) sv)]
      [(- (* s cos) (* t sin)) (+ (* s sin) (* t cos))]))