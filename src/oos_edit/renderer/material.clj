;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.renderer.material
   (:import
      (javafx.scene.image Image)
      (javafx.scene.paint PhongMaterial)))


(def ^:private ^:const +material-path+ "materials/")
(defonce ^:private ^:dynamic *material-cache* (volatile! (hash-map)))


;;;
;;; width
;;;

(defn width
   [material]
   (.getWidth (.getDiffuseMap material)))


;;;
;;; height
;;;

(defn height
   [material]
   (.getHeight (.getDiffuseMap material)))


;;;
;;; create
;;;

(defn- create
   [^String file-name]
   (let [diffuse-map (Image. file-name)]
      (doto (PhongMaterial.)
         (.setDiffuseMap diffuse-map))))


;;;
;;; cache
;;;

(defn cache
   [^String file-name]
   (let [prev (get @*material-cache* file-name)]  ; TODO: this is not thread-safe
      (if (nil? prev)
         (let [material (create (str +material-path+ file-name))]
            (println (str "Material: " file-name))
            (vswap! *material-cache* #(assoc % file-name material))
            material)
         prev)))