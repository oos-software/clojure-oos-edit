;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.renderer.frustum
   (:require
      [oos-edit.math.vector :as vector],
      [oos-edit.math.utils :as utils])
   (:import
      (oos_edit.math.vector Vector3)))


;;;
;;; create
;;;

(defn create
   [^Vector3 view-origin ^Vector3 view-right ^Vector3 view-normal ^Vector3 view-up ^double horizontal-fov ^double aspect-ratio]
   (let [a (utils/degrees->radians (* horizontal-fov 0.5))
         hs (Math/tan a)
         vs (/ hs aspect-ratio)]
      ))