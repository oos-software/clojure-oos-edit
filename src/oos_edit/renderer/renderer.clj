;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.renderer.renderer
   (:require
      [oos-edit.math.vector :as vector]
      [oos-edit.math.plane :as plane]
      [oos-edit.brush :as brush]
      [oos-edit.renderer.texture :as texture]
      [oos-edit.renderer.material :as material])
   (:import
      (javafx.scene.shape TriangleMesh MeshView CullFace Box DrawMode Sphere Cylinder)
      (javafx.scene DepthTest)
      (javafx.scene.paint PhongMaterial Color)
      (javafx.geometry Point3D)))


; TODO: create only one mesh for multiple views


;;;
;;; create-mesh-view
;;;

(defn- create-mesh-view
   [^TriangleMesh mesh material]
   (doto (MeshView.)
      (.setMesh mesh)
      (.setMaterial material)
      (.setCullFace CullFace/NONE)))


;;;
;;; winding->vertexes
;;;

(defn- winding->vertexes
   [points]
   (float-array (flatten (map (fn [v] [(:x v) (:y v) (:z v)]) points))))


;;;
;;; winding->texture-coordinates
;;;

(defn- winding->texture-coordinates
   [plane points translation-u translation-v scale-u scale-v rotation mode material]
   (let [width (material/width material)
         height (material/height material)
         calc (fn [vertex]
                 (texture/calculate-texture-coordinates vertex (:normal plane) width height translation-u translation-v scale-u scale-v rotation mode))]
      (float-array (flatten (map calc points)))))


;;;
;;; winding->faces
;;;

(defn- winding->faces
   [points]
   (int-array (loop [i 2
                     result []]
                 (if (< i (count points))
                    (recur (inc i)
                           (conj result 0 0 (dec i) (dec i) i i))
                    result))))


;;;
;;; brush-side->triangle-mesh
;;;

(defn- brush-side->triangle-mesh
   [side plane points material]
   (let [vertexes (winding->vertexes points)
         [tu tv] [(:translation-u side) (:translation-v side)]
         [su sv] [(:scale-u side) (:scale-v side)]
         rotation (:rotation side)
         mode (:mode side)
         texture-coordinates (winding->texture-coordinates plane points tu tv su sv rotation mode material)
         faces (winding->faces points)
         mesh (TriangleMesh.)]
      (.addAll (.getPoints mesh) vertexes)
      (.addAll (.getTexCoords mesh) texture-coordinates)
      (.addAll (.getFaces mesh) faces)
      mesh))


;;;
;;; brush-side->mesh-view
;;;

(defn- brush-side->mesh-view
   [side plane points]
   (let [material (material/cache (:material side))
         mesh (brush-side->triangle-mesh side plane points material)]
      (create-mesh-view mesh material)))


;;;
;;; brush->mesh-views
;;;

(defn brush->mesh-views
   [brush]
   (let [planes (brush/create-planes brush)
         windings (brush/create-windings brush)]
      (loop [i 0
             result []]
         (if (< i (count planes))
            (recur (inc i)
                   (conj result (brush-side->mesh-view (nth (:sides brush) i) (planes i) (windings i))))
            result))))


;;;
;;; create-drawable-brush-side
;;;

(defn- create-drawable-brush-side
   [side plane points]
   (let [material (material/cache (:material side))]
      {:material material
       :mesh (brush-side->triangle-mesh side plane points material)}))


;;;
;;; create-drawable-brush
;;;

(defn create-drawable-brush
   [brush]
   (let [planes (brush/create-planes brush)
         windings (brush/create-windings brush)]
      {:sides (map (fn [[side plane points]] (create-drawable-brush-side side plane points)) (:sides brush) planes windings)}))


;;;
;;; drawable-brush->mesh-views
;;;

(defn drawable-brush->mesh-views
   [drawable]
   (map #(create-mesh-view (:material %) (:mesh %)) (:sides drawable)))


;;;
;;; create-sphere
;;;

(defn- create-sphere
   [origin radius]
   (let [material (PhongMaterial.)]
      (.setDiffuseColor material (Color/GREEN))
      (doto (Sphere. radius)
         (.setMaterial material)
         (.setDepthTest DepthTest/DISABLE)
         (.setTranslateX (:x origin))
         (.setTranslateY (:y origin))
         (.setTranslateZ (:z origin)))))


;;;
;;; create-cylinder
;;;

(defn- create-cylinder
   [origin radius height]
   (let [material (PhongMaterial.)]
      (.setDiffuseColor material (Color/GREEN))
      (doto (Cylinder. radius height)
         (.setMaterial material)
         (.setDepthTest DepthTest/DISABLE)
         (.setTranslateX (:x origin))
         (.setTranslateY (:y origin))
         (.setTranslateZ (:z origin)))))


;;;
;;; brush->box
;;;

(defn brush->box
   [brush]
   (let [cylinder-radius 1.0
         sphere-radius 2.5
         [min max] (brush/bounding brush)
         center (vector/scale (vector/add min max) 0.5)
         size (vector/subtract max min)
         cylinder-0 (create-cylinder (vector/->Vector3 (:x min) (:y center) (:z min)) cylinder-radius (:y size))
         cylinder-1 (create-cylinder (vector/->Vector3 (:x min) (:y center) (:z max)) cylinder-radius (:y size))
         cylinder-2 (create-cylinder (vector/->Vector3 (:x max) (:y center) (:z min)) cylinder-radius (:y size))
         cylinder-3 (create-cylinder (vector/->Vector3 (:x max) (:y center) (:z max)) cylinder-radius (:y size))
         cylinder-4 (create-cylinder (vector/->Vector3 (:x center) (:y min) (:z min)) cylinder-radius (:x size))
         cylinder-5 (create-cylinder (vector/->Vector3 (:x center) (:y max) (:z min)) cylinder-radius (:x size))
         cylinder-6 (create-cylinder (vector/->Vector3 (:x center) (:y min) (:z max)) cylinder-radius (:x size))
         cylinder-7 (create-cylinder (vector/->Vector3 (:x center) (:y max) (:z max)) cylinder-radius (:x size))
         cylinder-8 (create-cylinder (vector/->Vector3 (:x min) (:y min) (:z center)) cylinder-radius (:z size))
         cylinder-9 (create-cylinder (vector/->Vector3 (:x max) (:y min) (:z center)) cylinder-radius (:z size))
         cylinder-10 (create-cylinder (vector/->Vector3 (:x max) (:y max) (:z center)) cylinder-radius (:z size))
         cylinder-11 (create-cylinder (vector/->Vector3 (:x min) (:y max) (:z center)) cylinder-radius (:z size))
         sphere-0 (create-sphere min sphere-radius)
         sphere-1 (create-sphere max sphere-radius)
         sphere-2 (create-sphere (vector/->Vector3 (:x max) (:y min) (:z min)) sphere-radius)
         sphere-3 (create-sphere (vector/->Vector3 (:x max) (:y min) (:z max)) sphere-radius)
         sphere-4 (create-sphere (vector/->Vector3 (:x max) (:y max) (:z min)) sphere-radius)
         sphere-5 (create-sphere (vector/->Vector3 (:x min) (:y max) (:z min)) sphere-radius)
         sphere-6 (create-sphere (vector/->Vector3 (:x min) (:y max) (:z max)) sphere-radius)
         sphere-7 (create-sphere (vector/->Vector3 (:x min) (:y min) (:z max)) sphere-radius)]
      (.setRotationAxis cylinder-4 (Point3D. 0.0 0.0 1.0))
      (.setRotate cylinder-4 90.0)
      (.setRotationAxis cylinder-5 (Point3D. 0.0 0.0 1.0))
      (.setRotate cylinder-5 90.0)
      (.setRotationAxis cylinder-6 (Point3D. 0.0 0.0 1.0))
      (.setRotate cylinder-6 90.0)
      (.setRotationAxis cylinder-7 (Point3D. 0.0 0.0 1.0))
      (.setRotate cylinder-7 90.0)
      (.setRotationAxis cylinder-8 (Point3D. 1.0 0.0 0.0))
      (.setRotate cylinder-8 90.0)
      (.setRotationAxis cylinder-9 (Point3D. 1.0 0.0 0.0))
      (.setRotate cylinder-9 90.0)
      (.setRotationAxis cylinder-10 (Point3D. 1.0 0.0 0.0))
      (.setRotate cylinder-10 90.0)
      (.setRotationAxis cylinder-11 (Point3D. 1.0 0.0 0.0))
      (.setRotate cylinder-11 90.0)
      [cylinder-0, cylinder-1, cylinder-2, cylinder-3, cylinder-4, cylinder-5, cylinder-6, cylinder-7, cylinder-8, cylinder-9, cylinder-10, cylinder-11,
       sphere-0, sphere-1, sphere-2, sphere-3, sphere-4, sphere-5, sphere-6, sphere-7]))