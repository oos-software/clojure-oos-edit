;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.menu
   (:import
      (javafx.scene.control Menu MenuBar MenuItem SeparatorMenuItem CheckMenuItem)
      (javafx.scene.input KeyCombination)
      (javafx.event EventHandler)))


;;;
;;; event-handler
;;;

(defn- event-handler
   [handler event]
   (reify EventHandler
      (handle [_ _]
         (handler {:event event}))))


;;;
;;; create-menu-item
;;;

(defn- create-menu-item
   [handler text accelerator event]
   (let [item (MenuItem. text)]
      (.setOnAction item (event-handler handler event))
      (when (> (.length accelerator) 0)
         (.setAccelerator item (KeyCombination/keyCombination accelerator)))
      item))


;;;
;;; create-check-menu-item
;;;

(defn- create-check-menu-item
   [handler text accelerator checked event]
   (let [item (CheckMenuItem. text)]
      (.setSelected item checked)
      (.setOnAction item (event-handler handler event))
      (when (> (.length accelerator) 0)
         (.setAccelerator item (KeyCombination/keyCombination accelerator)))
      item))


;;;
;;; create-menu
;;;

(defn- create-menu
   [text items]
   (let [menu (Menu. text)]
      (doseq [x items] (.add (.getItems menu) x))
      menu))


;;;
;;; create-editor-menu
;;;

(defn- create-editor-menu
   [handler]
   (create-menu "Editor" [(create-menu-item handler "Settings..." "" :editor/settings)
                          (SeparatorMenuItem.)
                          (create-menu-item handler "About..." "" :editor/about)
                          (SeparatorMenuItem.)
                          (create-menu-item handler "Quit" "Alt+F4" :editor/quit)]))


;;;
;;; create-export-menu
;;;

(defn- create-export-menu
   [handler]
   (create-menu "Export" [(create-menu-item handler "oos-tech..." "" :export/oos-tech)]))


;;;
;;; create-world-menu
;;;

(defn- create-world-menu
   [handler]
   (create-menu "World" [(create-menu-item handler "New..." "Ctrl+N" :world/new)
                         (create-menu-item handler "Open..." "Ctrl+O" :world/open)
                         (SeparatorMenuItem.)
                         (create-menu-item handler "Close" "Ctrl+Q" :world/close)
                         (SeparatorMenuItem.)
                         (create-menu-item handler "Save" "Ctrl+S" :world/save)
                         (create-menu-item handler "Save as..." "Ctrl+Shift+S" :world/save-as)
                         (SeparatorMenuItem.)
                         (create-export-menu handler)
                         (SeparatorMenuItem.)
                         (create-menu-item handler "Compile..." "" :world/compile)
                         (SeparatorMenuItem.)
                         (create-menu-item handler "Settings..." "" :world/settings)
                         (SeparatorMenuItem.)
                         (create-menu-item handler "Rebuild geometry" "F5" :world/rebuild-geometry)]))


;;;
;;; create-edit-menu
;;;

(defn- create-edit-menu
   [handler]
   (create-menu "Edit" [(create-menu-item handler "Undo" "Ctrl+Z" :edit/undo)
                        (create-menu-item handler "Redo" "Ctrl+Y" :edit/redo)]))


;;;
;;; create-brush-menu
;;;

(defn- create-brush-menu
   [handler]
   (create-menu "Brush" [(create-menu-item handler "To additive" "Alt+A" :brush/to-additive)
                         (create-menu-item handler "To subtractive" "Alt+S" :brush/to-subtractive)
                         (SeparatorMenuItem.)
                         (create-menu-item handler "To first" "Alt+1" :brush/to-first)
                         (create-menu-item handler "To last" "Alt+2" :brush/to-last)
                         (SeparatorMenuItem.)
                         (create-menu-item handler "To entity..." "Alt+E" :brush/to-entity)]))


;;;
;;; create-snapping-menu
;;;

(defn- create-snapping-menu
   [handler]
   (let [geometry (create-menu "Geometry" [(create-check-menu-item handler "1" "Ctrl+1" false :geometry-snapping/one)
                                           (create-check-menu-item handler "2" "Ctrl+2" false :geometry-snapping/two)
                                           (create-check-menu-item handler "4" "Ctrl+3" false :geometry-snapping/four)
                                           (create-check-menu-item handler "8" "Ctrl+4" false :geometry-snapping/eight)
                                           (create-check-menu-item handler "16" "Ctrl+5" true :geometry-snapping/sixteen)
                                           (create-check-menu-item handler "32" "Ctrl+6" false :geometry-snapping/thirty-two)])
         rotation (create-menu "Rotation" [(create-check-menu-item handler "1" "" false :rotation-snapping/one)
                                           (create-check-menu-item handler "15" "" true :rotation-snapping/fifteen)
                                           (create-check-menu-item handler "30" "" false :rotation-snapping/thirty)
                                           (create-check-menu-item handler "45" "" false :rotation-snapping/forty-five)
                                           (create-check-menu-item handler "90" "" false :rotation-snapping/ninety)])]
      (create-menu "Snapping" [geometry rotation])))


;;;
;;; create-menu-bar
;;;

(defn create-menu-bar
   [handler]
   (let [menu-bar (MenuBar.)
         editor (create-editor-menu handler)
         world (create-world-menu handler)
         edit (create-edit-menu handler)
         brush (create-brush-menu handler)
         snapping (create-snapping-menu handler)
         menus (into-array Menu [editor world edit brush snapping])]
      (.addAll (.getMenus menu-bar) menus)
      menu-bar))