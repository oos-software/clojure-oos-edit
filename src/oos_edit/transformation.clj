;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.transformation
   (:require
      [oos-edit.math.vector :as vector]
      [oos-edit.math.plane :as plane]
      [oos-edit.position :as position]
      [oos-edit.editor-misc :as misc]))


(def ^:private ^:dynamic *is-transformation* (volatile! false))
(def ^:private ^:dynamic *start-origin* (volatile! nil))
(def ^:private ^:dynamic *start-position* (volatile! nil))
;(def ^:private ^:dynamic *last-valid-position* (volatile! nil))  ; this is used when we can't intersect the ray with the plane (for edge cases)
(def ^:private ^:dynamic *transformation-plane* (volatile! nil))


;;;
;;; is-transformation
;;;

(defn is-transformation
   []
   @*is-transformation*)


;;;
;;; plane-from-position
;;;

(defn- plane-from-position
   [position]
   (plane/from-origin-and-normal position (vector/->Vector3 0.0 0.0 1.0)))


;;;
;;; begin
;;;

(defn begin
   [origin position ray-origin ray-direction]
   (let [plane (plane-from-position position)
         dist (plane/intersect-ray plane ray-origin ray-direction)]
      (when (>= dist 0.0)
         (vreset! *is-transformation* true)
         (vreset! *start-origin* origin)
         (vreset! *start-position* (vector/add ray-origin (vector/scale ray-direction dist)))
         (vreset! *transformation-plane* plane))))


;;;
;;; finish
;;;

(defn finish
   []
   (vreset! *is-transformation* false))


;;;
;;; snap-translation
;;;

(defn- snap-translation
   [translation snapping]
   (if (<= snapping 1)
      translation
      (position/snap translation snapping)))


;;;
;;; get-translation
;;;

(defn get-translation
   [ray-origin ray-direction]
   (let [dist (plane/intersect-ray @*transformation-plane* ray-origin ray-direction)]
      (when (>= dist 0.0)
         (let [snapping (misc/get-geometry-snapping false)
               position (vector/add ray-origin (vector/scale ray-direction dist))
               translation (vector/subtract position @*start-position*)
               translation (snap-translation (assoc (position/from-vector translation) :z 0) snapping)]
            (position/add @*start-origin* translation)))))
