;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.settings)


(def ^:private ^:const +settings-file-name+ "oos-edit.settings")


;;;
;;; create
;;;

(defn create
   []
   {:language                      :de
    :move-speed                    1024.0
    :move-shift-speed              8192.0
    :mouse-sensitivity             0.2
    :mouse-middle-sensitivity      2.5
    :orthographic-background-color "111111"
    :perspective-background-color  "000000"})


;;;
;;; persist
;;;

(defn persist
   [settings]
   )


;;;
;;; restore
;;;

(defn restore
   [settings]
   )