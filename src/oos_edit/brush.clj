;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.brush
   (:require
      [oos-edit.math.plane :as plane]
      [oos-edit.math.vector :as vector]
      [oos-edit.math.quaternion :as quaternion]
      [oos-edit.position :as position]
      [oos-edit.winding :as winding]))


(def ^:const +base-size+ 262144.0)                          ; base size of the windings TODO: is this the correct place? (move to project?)


;;;
;;; create-side
;;;
;;; offset = offset from brush origin
;;; mode can be eiter :world (aligned to world axes) or :face (aligned to the face)
;;;

(defn create-side
   [offset normal translation-u translation-v scale-u scale-v rotation mode ^String material]
   {:offset        offset
    :normal        normal
    :translation-u translation-u
    :translation-v translation-v
    :scale-u       scale-u
    :scale-v       scale-v
    :rotation      rotation
    :mode          mode
    :material      material})


;;;
;;; create
;;;
;;; mode can be either :additive (default) or :subtractive. entities other than the world can only have :additive brushes
;;;

(defn create
   [origin orientation sides mode]
   {:origin      origin
    :orientation orientation
    :sides       sides
    :mode        mode})


;;;
;;; side->plane
;;;

(defn- side->plane
   [brush side]
   (let [origin (position/add (:origin brush) (:offset side))]
      (plane/from-origin-and-normal (position/to-vector origin) (:normal side))))


;;;
;;; side->winding
;;;

(defn- side->winding
   [brush side]
   (let [origin (position/add (:origin brush) (:offset side))]
      (winding/base-winding (position/to-vector origin) (:normal side) +base-size+)))


;;;
;;; create-planes
;;;

(defn create-planes
   [brush]
   (vec (map #(side->plane brush %) (:sides brush))))


;;;
;;; side->plane
;;;

(defn create-windings
   [brush]
   (let [planes (create-planes brush)
         windings (vec (map #(side->winding brush %) (:sides brush)))]
      (loop [i 0
             result []]
         (if (< i (count planes))
            (let [winding (loop [j 0
                                 winding (windings i)]
                             (if (< j (count planes))
                                (recur (inc j)
                                       (if (= i j)
                                          winding
                                          (winding/chop-winding winding (planes j))))
                                winding))]
               (recur (inc i)
                      (conj result winding)))
            result))))


;;;
;;; bounding
;;;

(defn bounding
   [brush]
   (let [windings (create-windings brush)
         boundings (map winding/bounding windings)
         mins (map first boundings)
         maxs (map second boundings)
         min-comp (fn [i] (apply min (map #(vector/get-component % i) mins)))
         max-comp (fn [i] (apply max (map #(vector/get-component % i) maxs)))]
      [(vector/->Vector3 (min-comp 0) (min-comp 1) (min-comp 2))
       (vector/->Vector3 (max-comp 0) (max-comp 1) (max-comp 2))]))


;;;
;;; distance-from-side
;;;

(defn distance-from-side
   [brush side point]
   (let [origin (position/to-vector (position/add (:origin brush) (:offset side)))]
      (vector/scalar-product (:normal side) (vector/subtract point origin))))


;;;
;;; find-side
;;;

(defn find-side
   [brush normal]
   (let [index-dots (map-indexed (fn [index side] [index (vector/scalar-product (:normal side) normal)]) (:sides brush))
         sorted (sort-by second > index-dots)]
      (first (first sorted))))


;;;
;;; change-material
;;;

(defn change-material
   [brush ^String material]
   (let [new-sides (map #(assoc % :material material) (:sides brush))]
      (assoc brush :sides new-sides)))


;;;
;;; change-side-material
;;;

(defn change-side-material
   [brush index ^String material]
   (let [new-sides (map-indexed (fn [i s] (if (= index i) (assoc s :material material) s)) (:sides brush))]
      (assoc brush :sides new-sides)))


;;;
;;; translate
;;;

(defn translate
   [brush offset]
   (let [origin (position/add (:origin brush) offset)]
      (assoc brush :origin origin)))


;;;
;;; translate-side
;;;

(defn translate-side
   [brush index offset]
   (let [new-sides (map-indexed (fn [i s] (if (= index i) (assoc s :offset (position/add (:offset s) offset)) s)) (:sides brush))]
      (assoc brush :sides new-sides)))


;;;
;;; rotate
;;;

(defn rotate
   [brush rotation]
   (let [orientation (quaternion/normalize (quaternion/multiply (:orientation brush) rotation))]
      (assoc brush :orientation orientation)))


;;;
;;; point-inside?
;;;

(defn point-inside?
   [brush point ^double epsilon]
   (let [sides (:sides brush)]
      (loop [i 0]
         (if (< i (count sides))
            (if (> (distance-from-side brush (sides i) point) epsilon)
               false
               (recur (inc i)))
            true))))


;;;
;;; ray-outside-bounding?
;;;

(defn- ray-outside-bounding?
   [brush origin direction]
   (let [sides (:sides brush)]
      (loop [i 0]
         (if (< i (count sides))
            (let [side (nth sides i)
                  dist (distance-from-side brush side origin)]
               (if (and (>= dist 0.0) (>= (vector/scalar-product (:normal side) direction) 0.0))
                  true
                  (recur (inc i))))
            false))))


;;;
;;; ray-enter-fraction
;;;

(defn- ray-enter-fraction
   [brush origin direction]
   (let [sides (:sides brush)
         temp (loop [i 0 fractions [Double/NEGATIVE_INFINITY]]
                 (if (< i (count sides))
                    (let [side (nth sides i)
                          dist (distance-from-side brush side origin)
                          dot (vector/scalar-product (:normal side) direction)]
                       (if (and (> dist 0.0) (< dot -0.0001))
                          (recur (inc i)
                                 (conj fractions (/ dist (- dot))))
                          (recur (inc i)
                                 fractions)))
                    fractions))]
      (apply max temp)))


;;;
;;; ray-leave-fraction
;;;

(defn- ray-leave-fraction
   [brush origin direction]
   (let [sides (:sides brush)
         temp (loop [i 0 fractions [Double/POSITIVE_INFINITY]]
                 (if (< i (count sides))
                    (let [side (nth sides i)
                          dist (distance-from-side brush side origin)
                          dot (vector/scalar-product (:normal side) direction)]
                       (if (and (< dist 0.0) (> dot 0.0001))
                          (recur (inc i)
                                 (conj fractions (/ dist (- dot))))
                          (recur (inc i)
                                 fractions)))
                    fractions))]
      (apply min temp)))


;;;
;;; intersect-ray
;;;

(defn intersect-ray
   [brush origin direction]
   ;; first check if we're in front of a single side and pointing into the same direction
   (if (ray-outside-bounding? brush origin direction)
      -1.0
      (let [enter (ray-enter-fraction brush origin direction)
            leave (ray-leave-fraction brush origin direction)]
         (if (and (>= enter 0.0) (< enter leave))
            enter
            -1.0))))