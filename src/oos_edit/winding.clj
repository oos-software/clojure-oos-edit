;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.winding
   (:require
      [oos-edit.math.vector :as vector]
      [oos-edit.math.plane :as plane])
   (:import
      (oos_edit.math.vector Vector3)
      (oos_edit.math.plane Plane)))


;;;
;;; base-winding
;;;

(defn base-winding
   [^Vector3 origin ^Vector3 normal ^double base-size]
   (let [u (vector/perpendicular normal)
         r (vector/normalize (vector/cross-product u normal))]
      (let [up (vector/scale u base-size)
            right (vector/scale r base-size)]
         (let [left-point (vector/subtract origin right)
               right-point (vector/add origin right)]
            (let [point-0 (vector/subtract left-point up)
                  point-1 (vector/subtract right-point up)
                  point-2 (vector/add right-point up)
                  point-3 (vector/add left-point up)]
               [point-0 point-1 point-2 point-3])))))


;;;
;;; winding-edges
;;;

(defn- winding-edges
   [points]
   (vec (map vector points (concat (drop 1 points) (take 1 points)))))


;;;
;;; clip-winding-edge
;;;

(defn- clip-winding-edge
   [[edge-1 edge-2] ^Plane plane]
   (let [dist-1 (plane/distance-from plane edge-1)
         dist-2 (plane/distance-from plane edge-2)]
      (if (and (> dist-1 0.0) (> dist-2 0.0))
         []
         (conj (if (> dist-1 0.0)
                  []
                  [edge-1])
               (if (or (and (> dist-1 0.0) (< dist-2 0.0)) (and (< dist-1 0.0) (> dist-2 0.0)))
                  (let [dir (vector/subtract edge-2 edge-1)
                        frac (/ (- dist-1) (- dist-2 dist-1))]
                     [(vector/add edge-1 (vector/scale dir frac))])
                  [])))))


;;;
;;; chop-winding
;;;
;;; behind the plane is kept
;;;

(defn chop-winding
   [points ^Plane plane]
   (let [edges (winding-edges points)]
      (loop [counter 0
             result []]
         (if (< counter (count edges))
            (recur (inc counter)
                   (conj result (clip-winding-edge (edges counter) plane)))
            (vec (flatten result))))))


;;;
;;; split-winding
;;;

(defn split-winding
   [points ^Plane plane]
   (let [front (chop-winding points (plane/inverse plane))
         back (chop-winding points plane)]
      [front back]))


;;;
;;; bounding
;;;

(defn bounding
   [points]
   (let [comp (fn [i] (map (fn [vec] (vector/get-component vec i)) points))
         min-comp (fn [i] (apply min (comp i)))
         max-comp (fn [i] (apply max (comp i)))
         min-x (min-comp 0)
         min-y (min-comp 1)
         min-z (min-comp 2)
         max-x (max-comp 0)
         max-y (max-comp 1)
         max-z (max-comp 2)]
      [(vector/->Vector3 min-x min-y min-z) (vector/->Vector3 max-x max-y max-z)]))