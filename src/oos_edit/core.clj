;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.core
   (:require
      [oos-edit.defs :as defs]
      [oos-edit.project :as project]
      [oos-edit.menu :as menu]
      [oos-edit.control.tool-bar :as tool-bar]
      [oos-edit.control.status-bar :as status-bar]
      [oos-edit.control.content-bar :as content-bar]
      [oos-edit.control.tab-pane :as tab-pane]
      [oos-edit.events :as events]
      [oos-edit.keyboard :as keyboard]
      [oos-edit.editor :as editor])
   (:import
      (javafx.application Platform)
      (javafx.scene Scene)
      (javafx.stage Stage)
      (javafx.scene.layout VBox Priority AnchorPane)
      (javafx.embed.swing JFXPanel)
      (javafx.scene.control Alert Alert$AlertType ButtonType)
      (javafx.scene.input KeyCombination KeyCodeCombination KeyCode KeyCombination$Modifier KeyEvent)
      (javafx.event EventHandler Event)))


(defonce force-toolkit-init (JFXPanel.))                    ; this is just to force initialization of the JavaFX toolkit


;;; project hack
;(oos-edit.project/define-project "test" "resources/material/" 64.0
;                                 #{:world :light :liquid :trigger :clip}
;                                 #{})
;(oos-edit.project/use-project "test")


;;;
;;; on-close-request
;;;

(defn- on-close-request
   [event]
   (let [alert (Alert. Alert$AlertType/CONFIRMATION)
         save-button (ButtonType. "Save")
         discard-button (ButtonType. "Discard")]
      (.setTitle alert "Unsaved worlds")
      (.setHeaderText alert "Do you want to save?")
      (.setContentText alert "There are unsaved worlds.\nDo you want to save them before exiting the application?\n")
      (.clear (.getButtonTypes alert))
      (.addAll (.getButtonTypes alert) [save-button discard-button ButtonType/CANCEL])
      (let [result (.get (.showAndWait alert))]
         (cond
            (= result ButtonType/CLOSE) (.consume event)
            (= result ButtonType/CANCEL) (.consume event)
            (= (.getText result) "Save") (editor/save-open-worlds))))) ; pun intended


;;;
;;; keyboard-event-handler
;;;

(defn- keyboard-event-handler
   [keyboard-state callback]
   (reify EventHandler
      (handle [_ event]
         (callback keyboard-state event))))


;;;
;;; create-root-scene
;;;

(defn- create-root-scene
   [keyboard-state]
   (let [v-box (VBox.)
         scene (Scene. v-box)
         anchor-pane (AnchorPane.)
         menu-bar (menu/create-menu-bar events/handle-menu-event)
         main-tool-bar (tool-bar/create-main-tool-bar events/handle-menu-event)
         left-tool-bar (tool-bar/create-left-tool-bar events/handle-menu-event)
         content-bar (content-bar/create-content-bar)
         status-bar (status-bar/create-status-bar)
         tab-pane (tab-pane/create-tab-pane)]
      (.setFillWidth v-box true)
      (.setPrefSize v-box 1280.0 720.0)
      (AnchorPane/setLeftAnchor left-tool-bar 0.0)
      (AnchorPane/setTopAnchor left-tool-bar 0.0)
      (AnchorPane/setBottomAnchor left-tool-bar 0.0)
      (AnchorPane/setRightAnchor content-bar 0.0)
      (AnchorPane/setTopAnchor content-bar 0.0)
      (AnchorPane/setBottomAnchor content-bar 0.0)
      (AnchorPane/setLeftAnchor tab-pane 42.0)
      (AnchorPane/setRightAnchor tab-pane (+ 256.0 2.0 20.0))  ; TODO!
      (AnchorPane/setTopAnchor tab-pane 0.0)
      (AnchorPane/setBottomAnchor tab-pane 0.0)
      (VBox/setVgrow anchor-pane Priority/ALWAYS)
      (.addAll (.getChildren anchor-pane) [left-tool-bar content-bar tab-pane])
      (.addAll (.getChildren v-box) [menu-bar main-tool-bar anchor-pane status-bar])
      ;KeyCombination kc = new KeyCodeCombination(KeyCode.Z, KeyCombination.CONTROL_DOWN);
      ;(let [kc (KeyCodeCombination. KeyCode/Z KeyCombination/NO_MATCH)
      ;      rn (reify Runnable
      ;            (run [_]
      ;               (println "Worked!")))]
      ;   (.put (.getAccelerators scene) kc rn))

      (let [kc (KeyCodeCombination. KeyCode/G (into-array KeyCombination$Modifier [KeyCombination/CONTROL_DOWN]));KeyCombination/SHORTCUT_DOWN]))
            rn (reify Runnable
                  (run [_]
                     (println "Worked!")))]
         (.put (.getAccelerators scene) kc rn))

      (.addEventFilter scene KeyEvent/KEY_PRESSED (keyboard-event-handler keyboard-state keyboard/on-key-pressed))
      (.addEventFilter scene KeyEvent/KEY_RELEASED (keyboard-event-handler keyboard-state keyboard/on-key-released))
      (.addEventHandler scene KeyEvent/KEY_PRESSED (reify EventHandler
                                                       (handle [_ event]
                                                          (.consume event))))
      (.addEventHandler scene KeyEvent/KEY_RELEASED (reify EventHandler
                                                      (handle [_ event]
                                                         (.consume event))))
      ;(.setOnKeyPressed scene (keyboard-event-handler keyboard-state keyboard/on-key-pressed))
      ;(.setOnKeyReleased scene (keyboard-event-handler keyboard-state keyboard/on-key-released))
      scene))


;;;
;;; create-root-stage
;;;

(defn- create-root-stage
   [keyboard-state]
   (doto (Stage.)
      (.setTitle (str defs/+oos-edit-name+ " (" defs/+oos-edit-version+ ")"))
      (.setMinWidth 640.0)
      (.setMinHeight 480.0)
      ;(.setOnCloseRequest (event-handler on-close-request)) ; TODO!
      (.setScene (create-root-scene keyboard-state))))


;;;
;;; run-application
;;;

(defn run-application
   [implicit-exit]
   (Platform/setImplicitExit implicit-exit)
   (Platform/runLater (fn []
                         (.showAndWait
                            (let [keyboard-state (volatile! (keyboard/create-state))
                                  stage (create-root-stage keyboard-state)]
                               (editor/initialize stage keyboard-state)
                               stage)))))


;;;
;;; -main
;;;

(defn -main
   [& args]
   (run-application true))