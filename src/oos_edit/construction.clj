;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.construction
   (:require
      [oos-edit.position :as position]))


(def ^:private ^:dynamic *is-construction* (volatile! false))
(def ^:private ^:dynamic *start-position* (volatile! position/+zero+))


;;;
;;; validate-positions
;;;

(defn- validate-positions
   [start end]
   (let [min-x (min (:x start) (:x end))
         min-y (min (:y start) (:y end))
         min-z (min (:z start) (:z end))
         max-x (max (:x start) (:x end))
         max-y (max (:y start) (:y end))
         max-z (max (:z start) (:z end))]
      [(position/create min-x min-y min-z)
       (position/create max-x max-y max-z)]))


;;;
;;; is-construction
;;;

(defn is-construction
   []
   @*is-construction*)


;;;
;;; begin
;;;

(defn begin
   [position snapping]
   (vreset! *is-construction* true)
   (vreset! *start-position* (position/snap position snapping)))


;;;
;;; finish
;;;

(defn finish
   [position snapping]
   (when (= @*is-construction* true)
      (vreset! *is-construction* false)
      (let [snap-start (position/snap @*start-position* snapping)
            snap-position (position/snap position snapping)]
         (validate-positions snap-start snap-position))))