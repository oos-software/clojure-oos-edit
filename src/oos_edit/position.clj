;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.position
   (:require
      [oos-edit.math.vector :as vector]))


(def ^:private ^:const +fraction-bits+ 10)                            ; 54.10 fixed-point
(def ^:private ^:const +fraction-scale+ (double (bit-shift-left 1 +fraction-bits+)))


;;;
;;; create
;;;

(defn create
   [^long x ^long y ^long z]
   {:x x
    :y y
    :z z})


;;;
;;; integer->fixed
;;;

(defn integer->fixed
   [^long value]
   (bit-shift-left value +fraction-bits+))


;;;
;;; from-integer
;;;

(defn from-integer
   [^long x ^long y ^long z]
   (create (integer->fixed x) (integer->fixed y) (integer->fixed z)))


;;;
;;; from-vector
;;;

(defn from-vector
   [vec]
   (let [convert #(* % +fraction-scale+)
         x (convert (:x vec))
         y (convert (:y vec))
         z (convert (:z vec))]
      (create x y z)))


;;;
;;; to-vector
;;;

(defn to-vector
   [pos]
   (let [x (double (:x pos))
         y (double (:y pos))
         z (double (:z pos))]
      (vector/div (vector/->Vector3 x y z) +fraction-scale+)))


;;;
;;; replace-component
;;;

(defn replace-component
   [pos coord value]
   (assoc pos coord value))


;;;
;;; add
;;;

(defn add
   [pos-1 pos-2]
   {:x (+ (:x pos-1) (:x pos-2))
    :y (+ (:y pos-1) (:y pos-2))
    :z (+ (:z pos-1) (:z pos-2))})


;;;
;;; subtract
;;;

(defn subtract
   [pos-1 pos-2]
   {:x (- (:x pos-1) (:x pos-2))
    :y (- (:y pos-1) (:y pos-2))
    :z (- (:z pos-1) (:z pos-2))})


;;;
;;; abs
;;;

(defn abs
   [pos]
   {:x (Math/abs ^long (:x pos))
    :y (Math/abs ^long (:y pos))
    :z (Math/abs ^long (:z pos))})


;;;
;;; snap
;;;

(defn snap
   [pos ^long size]
   (let [half (bit-shift-right size 1)
         value #(if (< % 0) (- % half) (+ % half))
         convert #(* (quot (value %) size) size)
         x (convert (:x pos))
         y (convert (:y pos))
         z (convert (:z pos))]
      {:x x :y y :z z}))


(def ^:const +zero+ {:x 0 :y 0 :z 0})
(def ^:const +one+ (from-integer 1 1 1))