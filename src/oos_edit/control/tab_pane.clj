;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.control.tab-pane
   (:import
      (javafx.scene.control TabPane Tab TabPane$TabClosingPolicy)))


(def ^:dynamic *tab-pane* (volatile! nil))
(def ^:dynamic *next-tab-id* (volatile! (long 0)))               ; always increasing, never reused
(def ^:dynamic *tab-data* (volatile! {}))


;;;
;;; event-handler
;;;

(defn- event-handler
   [callback]
   (reify javafx.event.EventHandler
      (handle [_ event]
         (callback event))))


;;;
;;; on-tab-close-request
;;;

(defn- on-tab-close-request
   [event]
   (vswap! *tab-data* dissoc (.getId (.getTarget event))))


;;;
;;; get-current-tab
;;;

(defn get-current-tab
   []
   (let [model (.getSelectionModel @*tab-pane*)]
      (if (.isEmpty model)
         nil
         (let [tab (.getSelectedItem model)
               data (get @*tab-data* (.getId tab))]
            [tab data]))))


;;;
;;; with-current-tab
;;;

(defmacro with-current-tab
   [[tab data] & body]
   `(let [temp# (get-current-tab)]
       (when temp#
          (let [[~tab ~data] temp#]
             ~@body))))


;;;
;;; for-all-tabs
;;;

(defn for-all-tabs
   [callback]
   (doseq [tab (.getTabs @*tab-pane*)]
      (let [data (get @*tab-data* (.getId tab))]
         (callback tab data))))


;;;
;;; add-tab
;;;

(defn add-tab
   [text state content]
   (let [tab (Tab.)
         id (str (vswap! *next-tab-id* inc))]
      (.setId tab id)
      (.setText tab text)
      (.setContent tab content)
      (.setOnCloseRequest tab (event-handler on-tab-close-request))
      (vswap! *tab-data* assoc id state)
      (.add (.getTabs @*tab-pane*) tab)
      (doto (.getSelectionModel @*tab-pane*)
         (.select (- (.size (.getTabs @*tab-pane*)) 1)))
      tab))


;;;
;;; create-tab-pane
;;;

(defn create-tab-pane
   []
   (let [tab-pane (TabPane.)]
      (.setTabClosingPolicy tab-pane TabPane$TabClosingPolicy/ALL_TABS)
      (vreset! *tab-pane* tab-pane)
      tab-pane))