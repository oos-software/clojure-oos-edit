;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.control.content-bar
   (:require
      [oos-edit.renderer.material :as material])
   (:import
      (javafx.scene.layout GridPane)
      (javafx.scene AmbientLight Group)
      (javafx.scene.image ImageView Image)
      (javafx.scene.control ScrollPane ScrollPane$ScrollBarPolicy)
      (javafx.scene.shape Rectangle)
      (javafx.scene.paint Paint)
      (javafx.event EventHandler)))


; TODO: support multiple tile sizes


(def ^:private ^:dynamic *rectangle* (volatile! nil))
(def ^:private ^:dynamic *materials* (volatile! (sorted-map)))
(def ^:private ^:dynamic *material-names* (volatile! []))

(def ^:private ^:dynamic *selected-material* (volatile! ""))

(def ^:private ^:const +material-directory+ "resources/materials/")  ; TODO: move to project


;;;
;;; update-rectangle
;;;

(defn- update-rectangle
   [col row]
   (let [rectangle @*rectangle*]
      (.setVisible rectangle true)
      (.setX rectangle (+ (* col 128.0) 2.0))
      (.setY rectangle (+ (* row 128.0) 2.0))))


;;;
;;; selected-material
;;;

(defn selected-material
   []
   @*selected-material*)


;;;
;;; set-fetched-material
;;;

(defn set-fetched-material
   [material]
   (let [rectangle @*rectangle*
         index (first (keep-indexed #(when (= %2 material) %1) @*material-names*))]
      (if (nil? index)
         (.setVisible rectangle false)
         (let [col (mod index 2)
               row (quot index 2)]
            (update-rectangle col row)))
      (vreset! *selected-material* material)))


;;;
;;; list-material-names
;;;

(defn- list-material-names
   []
   (let [directory (clojure.java.io/file +material-directory+)
         files (filter #(.isFile %) (file-seq directory))
         paths (map #(clojure.string/replace (subs (.getPath %) (count +material-directory+)) "\\" "/") files)]
      (filter #(not (clojure.string/starts-with? % "engine/")) paths)))


;;;
;;; add-material
;;;

(defn- add-material
   [^String path]
   (vreset! *materials* (assoc @*materials* path (material/cache path)))
   (vreset! *material-names* (conj @*material-names* path)))


;;;
;;; on-mouse-pressed
;;;

(defn- on-mouse-pressed
   [event]
   (let [x (.getX event)
         y (.getY event)
         row (quot y 128.0)
         col (if (>= x 128.0) 1 0)
         index (+ (* row 2) col)]
      (when (< index (count @*material-names*))
         (update-rectangle col row)
         (vreset! *selected-material* (nth @*material-names* index)))))


;;;
;;; create-content-bar
;;;

(defn create-content-bar
   []
   (let [content-bar (ScrollPane.)
         grid-pane (GridPane.)
         light (AmbientLight.)
         group (Group. [light])
         material-names (list-material-names)]
      (.add (.getStylesheets content-bar) "editor/css/content-bar.css")
      (.setHbarPolicy content-bar ScrollPane$ScrollBarPolicy/NEVER)
      (.setVbarPolicy content-bar ScrollPane$ScrollBarPolicy/ALWAYS)
      (.setFocusTraversable content-bar false)
      (.setFocusTraversable grid-pane false)
      (.setFocusTraversable group false)
      (doseq [name material-names] (add-material name))
      (doseq [[index [_ material]] (map-indexed vector @*materials*)]
         (let [view (ImageView. ^Image (.getDiffuseMap material))]
            (.setFitWidth view 128)
            (.setFitHeight view 128)
            (.add grid-pane view (mod index 2) (/ index 2))))
      (.add (.getChildren group) grid-pane)
      (let [rectangle (Rectangle. 2.0 2.0 124.0 124.0)]
         (.setFill rectangle nil)
         (.setStroke rectangle (Paint/valueOf "crimson"))
         (.setStrokeWidth rectangle 3.0)
         (.add (.getChildren group) rectangle)
         (vreset! *rectangle* rectangle))
      (vreset! *selected-material* (nth @*material-names* 0))
      (.setOnMousePressed content-bar (reify EventHandler
                                         (handle [_ event]
                                            (.consume event))))
      (.setOnMousePressed grid-pane (reify EventHandler
                                       (handle [_ event]
                                          (.consume event)
                                          (on-mouse-pressed event))))
      (.setContent content-bar group)
      content-bar))