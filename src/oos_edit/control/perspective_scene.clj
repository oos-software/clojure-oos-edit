;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.control.perspective-scene
   (:require
      [oos-edit.math.utils :as utils]
      [oos-edit.math.vector :as vector]
      [oos-edit.position :as position]
      [oos-edit.mouse :as mouse]
      [oos-edit.view :as view]
      [oos-edit.control.tab-pane :as tab-pane]
      [oos-edit.control.status-bar :as status-bar]
      [oos-edit.control.scene-utils :as scene-utils]
      [oos-edit.picking :as picking]
      [oos-edit.transformation :as transformation]
      [oos-edit.world :as world]
      [oos-edit.entity :as entity]
      [oos-edit.brush :as brush])
   (:import
      (javafx.scene SceneAntialiasing SubScene Group AmbientLight)
      (javafx.scene.paint Paint Color)
      (java.util Locale)
      (javafx.scene.text Text Font TextAlignment)
      (javafx.scene.shape StrokeType Rectangle)))


(def ^:private ^:const +mouse-sensitivity+ 0.2)
(def ^:private ^:const +mouse-middle-sensitivity+ 2.5)
(def ^:private ^:const +max-pitch+ 80.0)
(def ^:private ^:const +move-speed+ 1024.0)                 ; TODO: move to settings
(def ^:private ^:const +move-shift-speed+ 8192.0)           ; TODO: move to settings


;;;
;;; normalized-mouse-coordinates
;;;
;;; the mouse coordinates must be in local scene space
;;; this returns values from -1 to 1 and must be scaled by the fov later on
;;;

(defn normalized-mouse-coordinates
   [mouse-x mouse-y width height]
   (let [half-width (* width 0.5)
         half-height (* height 0.5)]
      [(/ (+ (- mouse-x half-width) 0.5) half-width)
       (- (/ (+ (- mouse-y half-height) 0.5) half-height))]))


;;;
;;; update-perspective-view-status-bar
;;;

(defn- update-perspective-view-status-bar
   [state]
   (let [view (:perspective-view @state)
         origin (vector/snap (:origin view) 0.1)
         rotation (vector/snap (:rotation view) 0.1)
         f (fn [x] (String/format Locale/US "%.1f" (into-array Object [x])))
         to-string (fn [value] (str (f (:x value)) ", " (f (:y value)) ", " (f (:z value))))]
      (status-bar/set-section-text :right (str (to-string origin) " / " (to-string rotation)))))


;;;
;;; build-movement-vector
;;;

(defn- build-movement-vector
   [keyboard-state delta-seconds]
   (let [state @keyboard-state
         left (:move-left state)
         right (:move-right state)
         backward (:move-backward state)
         forward (:move-forward state)
         down (:move-down state)
         up (:move-up state)
         speed (* (if (:speed state) +move-shift-speed+ +move-speed+) delta-seconds)
         x (+ (if left -1.0 0.0) (if right 1.0 0.0))
         y (+ (if backward -1.0 0.0) (if forward 1.0 0.0))
         z (+ (if down -1.0 0.0) (if up 1.0 0.0))]
      (if (and (= x 0.0) (= y 0.0) (= z 0.0))
         nil
         (vector/scale (vector/->Vector3 x y z) speed))))


;;;
;;; translate-perspective-view-internal
;;;

(defn- translate-perspective-view-internal
   [state camera movement straight-up]
   (let [view (:perspective-view @state)
         rotation (:rotation view)
         [right forward up] (view/calculate-vectors rotation)
         move-right (vector/scale right (vector/get-component movement 0))
         move-forward (vector/scale forward (vector/get-component movement 1))
         move-up (vector/scale (if straight-up (vector/->Vector3 0.0 0.0 1.0) up) (vector/get-component movement 2))
         move (vector/add (vector/add move-right move-forward) move-up)
         new-origin (vector/add (:origin view) move)]
      (reset! state (assoc @state :perspective-view (assoc view :origin new-origin)))
      (.clear (.getTransforms camera))
      (.addAll (.getTransforms camera) (view/build-transformation new-origin (:rotation view)))
      (update-perspective-view-status-bar state)))


;;;
;;; translate-perspective-view
;;;

(defn- translate-perspective-view
   [tab state movement]
   (let [scene (scene-utils/get-perspective-scene tab)
         camera (.getCamera scene)]
      (translate-perspective-view-internal state camera movement false)))


;;;
;;; rotate-perspective-view
;;;

(defn- rotate-perspective-view
   [state camera move-x move-y]
   (let [view (:perspective-view @state)
         rotation (:rotation view)
         x (utils/clamp (- (vector/get-component rotation 0) move-y) (- +max-pitch+) +max-pitch+)
         z (utils/clamp-angle (- (vector/get-component rotation 2) move-x))
         new-rotation (vector/->Vector3 x 0.0 z)]
      (reset! state (assoc @state :perspective-view (assoc view :rotation new-rotation)))
      (.clear (.getTransforms camera))
      (.addAll (.getTransforms camera) (view/build-transformation (:origin view) new-rotation))
      (update-perspective-view-status-bar state)))


;;;
;;; on-perspective-scene-mouse-pressed
;;;

(defn- on-perspective-scene-mouse-pressed
   [_ _ event state mouse-state]
   (.consume event)
   (let [x (.getX event)
         y (.getY event)
         button (mouse/event-button-name event)
         dragged (mouse/event-dragged-name event)
         start-pos (mouse/event-pos-name event)]
      (when (= :left-button button)
         (let [source (.getSource event)
               width (.getWidth source)
               height (.getHeight source)
               [mouse-x mouse-y] (normalized-mouse-coordinates x y width height)
               [ray-origin ray-direction] (view/perspective-picking-ray (:perspective-view @state) mouse-x mouse-y width height)]
            (if (.isShiftDown event)
               (scene-utils/perform-picking state ray-origin ray-direction)
               (let [result (:picking-result @state)]
                  (when (some? result)
                     (let [result (picking/current-result result)
                           brush (entity/get-brush (world/get-entity (:world @state) (:entity-id result)) (:brush-id result))]
                        (transformation/begin (:origin brush) (:hit-point result) ray-origin ray-direction)))))))
      (vswap! mouse-state assoc button true dragged false start-pos [x y])))


;;;
;;; on-perspective-scene-mouse-dragged
;;;

(defn- on-perspective-scene-mouse-dragged
   [_ scene event state mouse-state]
   (.consume event)
   (let [camera (.getCamera scene)
         x (.getX event)
         y (.getY event)
         ms @mouse-state]
      ;; left button
      (when (:left-button ms)
         (let [source (.getSource event)
               width (.getWidth source)
               height (.getHeight source)
               [mouse-x mouse-y] (normalized-mouse-coordinates x y width height)
               [ray-origin ray-direction] (view/perspective-picking-ray (:perspective-view @state) mouse-x mouse-y width height)]
            (when (transformation/is-transformation)
               (let [result (:picking-result @state)]
                  (when (some? result)
                     (let [result (picking/current-result result)
                           brush (entity/get-brush (world/get-entity (:world @state) (:entity-id result)) (:brush-id result))]
                        (scene-utils/update-brush-in-state state (:entity-id result) (:brush-id result)
                                                           (assoc brush :origin (transformation/get-translation ray-origin ray-direction)))))))))
      ;; right button
      (when (:right-button ms)
         (let [dragged (:right-dragged ms)
               [start-x start-y] (:right-pos ms)]
            (if dragged
               (let [move-x (* (- x start-x) +mouse-sensitivity+)
                     move-y (* (- y start-y) +mouse-sensitivity+)]
                  (rotate-perspective-view state camera move-x move-y)
                  (vswap! mouse-state assoc :right-pos [x y]))
               (when (or (>= (Math/abs (double (- start-x x))) scene-utils/+mouse-drag-distance+)
                         (>= (Math/abs (double (- start-y y))) scene-utils/+mouse-drag-distance+))
                  (vswap! mouse-state assoc :right-dragged true :right-pos [x y])))))
      ;; middle button
      (when (:middle-button ms)
         (let [[start-x start-y] (:middle-pos ms)
               move-x (* (- x start-x) +mouse-middle-sensitivity+)
               move-y (* (- y start-y) +mouse-middle-sensitivity+)]
            (translate-perspective-view-internal state camera (vector/->Vector3 move-x 0.0 (- move-y)) true)
            (vswap! mouse-state assoc :middle-pos [x y])))))


;;;
;;; on-perspective-scene-mouse-released
;;;

(defn- on-perspective-scene-mouse-released
   [_ _ event _ mouse-state]
   (.consume event)
   (let [button (mouse/event-button-name event)]
      (when (= :left-button button)
         (transformation/finish))
      (vswap! mouse-state assoc button false)))


;;;
;;; handle-keyboard
;;;

(defn handle-keyboard
   [keyboard-state delta-seconds]
   ;; translate perspective view
   (let [movement (build-movement-vector keyboard-state delta-seconds)]
      (when movement
         (tab-pane/with-current-tab [tab state]
                                    (translate-perspective-view tab state movement)))))


;;;
;;; create-overlay
;;;

(defn create-overlay
   []
   (let [light (AmbientLight.)
         group (Group. [light])
         rectangle (Rectangle. 300.0 40.0)
         text (Text. 100.0 100.0 "128x128 (1mx1m)")
         scene (SubScene. group 0.0 0.0 false SceneAntialiasing/DISABLED)]
      (.setTextAlignment text TextAlignment/CENTER)
      (.setFont text (Font. 20))
      (.setStroke text Color/FUCHSIA)
      (.setFill text Color/FUCHSIA)
      (.setFill rectangle Color/WHITE)
      (.setMouseTransparent text true)
      (.setTranslateX rectangle 100.0)
      (.setTranslateY rectangle 100.0)
      (.add (.getChildren group) rectangle)
      (.add (.getChildren group) text)
      scene))


;;;
;;; create
;;;

(defn create
   [stage state]
   (let [light (AmbientLight.)
         group (Group. [light])
         scene (SubScene. group 0.0 0.0 true SceneAntialiasing/BALANCED)]
      (.setFill scene (Paint/valueOf "black"))
      (.setCamera scene (view/create-perspective-camera))
      (let [mouse-state (volatile! (mouse/create-state))]
         (.setOnMousePressed scene (scene-utils/event-handler stage scene state mouse-state on-perspective-scene-mouse-pressed))
         (.setOnMouseDragged scene (scene-utils/event-handler stage scene state mouse-state on-perspective-scene-mouse-dragged))
         (.setOnMouseReleased scene (scene-utils/event-handler stage scene state mouse-state on-perspective-scene-mouse-released)))
      scene))


