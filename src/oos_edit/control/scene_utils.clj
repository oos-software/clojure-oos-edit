;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.control.scene-utils
   (:require
      [oos-edit.brush :as brush]
      [oos-edit.world :as world]
      [oos-edit.entity :as entity]
      [oos-edit.entities :as entities]
      [oos-edit.renderer.renderer :as renderer]
      [oos-edit.renderer.render-data :as render-data]
      [oos-edit.control.tab-pane :as tab-pane]
      [oos-edit.picking :as picking])
   (:import
      (java.awt MouseInfo)
      (javafx.event EventHandler)))


(def ^:const +mouse-drag-distance+ 5.0)


;;;
;;; event-handler
;;;

(defn event-handler
   [stage scene state event-state callback]
   (reify EventHandler
      (handle [_ event]
         (callback stage scene event state event-state))))


;;;
;;; get-orthographic-scene-and-grid
;;;

(defn get-orthographic-scene-and-grid
   [tab]
   (let [box (.get (.getItems (.getContent tab)) 0)
         stack-pane (.get (.getChildren box) 0)
         scene (.get (.getChildren stack-pane) 0)
         grid (.get (.getChildren stack-pane) 1)]
      [scene grid]))


;;;
;;; get-orthographic-overlay
;;;

(defn get-orthographic-overlay
   [tab]
   (let [box (.get (.getItems (.getContent tab)) 0)
         stack-pane (.get (.getChildren box) 0)
         overlay (.get (.getChildren stack-pane) 2)]
      overlay))


;;;
;;; get-perspective-scene
;;;

(defn get-perspective-scene
   [tab]
   (let [box (.get (.getItems (.getContent tab)) 1)
         stack-pane (.get (.getChildren box) 0)
         scene (.get (.getChildren stack-pane) 0)]
      scene))


;;;
;;; get-perspective-overlay
;;;

(defn get-perspective-overlay
   [tab]
   (let [box (.get (.getItems (.getContent tab)) 1)
         stack-pane (.get (.getChildren box) 0)
         overlay (.get (.getChildren stack-pane) 1)]
      overlay))


;;;
;;; mouse-inside-scene
;;;

(defn mouse-inside-scene
   [scene]
   (let [location (.getLocation (MouseInfo/getPointerInfo))
         point (.screenToLocal scene (.getX location) (.getY location))]
      (when (some? point) ; this can happen for freshly added tabs
         (let [px (.getX point)
               py (.getY point)
               width (.getWidth scene)
               height (.getHeight scene)]
            (and (>= px 0.0) (< px width) (>= py 0.0) (< py height))))))


;;;
;;; add-render-data-to-tab
;;;

(defn add-render-data-to-tab
   [tab orthographic-mesh-views perspective-mesh-views]
   (let [[orthographic _] (get-orthographic-scene-and-grid tab)
         perspective (get-perspective-scene tab)]
      (doseq [view orthographic-mesh-views]
         (.add (.getChildren (.getRoot orthographic)) view))
      (doseq [view perspective-mesh-views]
         (.add (.getChildren (.getRoot perspective)) view))))


;;;
;;; remove-render-data-from-tab
;;;

(defn remove-render-data-from-tab
   [tab orthographic-mesh-views perspective-mesh-views]
   (let [[orthographic _] (get-orthographic-scene-and-grid tab)
         perspective (get-perspective-scene tab)]
      (doseq [view orthographic-mesh-views]
         (.remove (.getChildren (.getRoot orthographic)) view))
      (doseq [view perspective-mesh-views]
         (.remove (.getChildren (.getRoot perspective)) view))))


;;;
;;; add-brush-render-data-to-state
;;;

(defn add-brush-render-data-to-state
   [state entity-id brush-id brush]
   (let [render-data-key (str entity-id "-" brush-id)
         orthographic-mesh-views (renderer/brush->mesh-views brush)
         perspective-mesh-views (renderer/brush->mesh-views brush)
         [new-orthographic-render-data new-perspective-render-data] (render-data/add-brush-render-data state render-data-key orthographic-mesh-views perspective-mesh-views)]
      (reset! state (assoc @state :orthographic-render-data new-orthographic-render-data
                                  :perspective-render-data new-perspective-render-data))
      [orthographic-mesh-views perspective-mesh-views]))



;;;
;;; add_brush_to_state
;;;

(defn add-brush-to-state
   [state entity-id brush]
   (let [entity (world/get-entity (:world @state) entity-id)
         [brush-id new-entity] (entity/add-brush entity brush)
         new-world (world/update-entity (:world @state) entity-id new-entity)
         render-data-key (str entity-id "-" brush-id)
         orthographic-mesh-views (renderer/brush->mesh-views brush)
         perspective-mesh-views (renderer/brush->mesh-views brush)
         [new-orthographic-render-data new-perspective-render-data] (render-data/add-brush-render-data state render-data-key orthographic-mesh-views perspective-mesh-views)]
      (tab-pane/with-current-tab [tab _]
                                 (add-render-data-to-tab tab orthographic-mesh-views perspective-mesh-views))
      (reset! state (assoc @state :world new-world
                                  :orthographic-render-data new-orthographic-render-data
                                  :perspective-render-data new-perspective-render-data))))


;;;
;;; remove-brush-from-state
;;;

(defn remove-brush-from-state
   [state entity-id brush-id]
   (let [entity (world/get-entity (:world @state) entity-id)
         new-entity (entity/remove-brush entity brush-id)
         new-world (world/update-entity (:world @state) entity-id new-entity)
         render-data-key (str entity-id "-" brush-id)
         [orthographic-mesh-views perspective-mesh-views new-orthographic-render-data new-perspective-render-data] (render-data/remove-brush-render-data state render-data-key)]
      (tab-pane/with-current-tab [tab _]
                                 (remove-render-data-from-tab tab orthographic-mesh-views perspective-mesh-views))
      (reset! state (assoc @state :world new-world
                                  :orthographic-render-data new-orthographic-render-data
                                  :perspective-render-data new-perspective-render-data))))


;;;
;;; update-brush-in-state
;;;

(defn update-brush-in-state
   [state entity-id brush-id brush]
   (let [entity (world/get-entity (:world @state) entity-id)
         new-entity (entity/update-brush entity brush-id brush)
         new-world (world/update-entity (:world @state) entity-id new-entity)
         render-data-key (str entity-id "-" brush-id)]
      (tab-pane/with-current-tab [tab _]
                                 (let [[orthographic-mesh-views perspective-mesh-views new-orthographic-render-data new-perspective-render-data] (render-data/remove-brush-render-data state render-data-key)]
                                    (remove-render-data-from-tab tab orthographic-mesh-views perspective-mesh-views)
                                    (reset! state (assoc @state :orthographic-render-data new-orthographic-render-data
                                                                :perspective-render-data new-perspective-render-data)))
                                 (let [orthographic-mesh-views (renderer/brush->mesh-views brush)
                                       perspective-mesh-views (renderer/brush->mesh-views brush)
                                       [new-orthographic-render-data new-perspective-render-data] (render-data/add-brush-render-data state render-data-key orthographic-mesh-views perspective-mesh-views)]
                                    (add-render-data-to-tab tab orthographic-mesh-views perspective-mesh-views)
                                    (reset! state (assoc @state :world new-world
                                                                :orthographic-render-data new-orthographic-render-data
                                                                :perspective-render-data new-perspective-render-data))))))


;;;
;;; add-entity-to-state
;;;

(defn add-entity-to-state
   [state class]
   (let [entity (entities/spawn class)]
      (println entity)))


;;;
;;; remove-entity-from-state
;;;

(defn remove-entity-from-state
   []
   )


;;;
;;; perform-picking
;;;

(defn perform-picking
   [state ray-origin ray-direction]
   ;; clear the previous result
   (when (some? (:picking-result @state))
      (let [current (picking/current-result (:picking-result @state))
            entity-id (:entity-id current)
            brush-id (:brush-id current)
            render-data-key (str entity-id "-" brush-id)
            [orthographic-box perspective-box new-orthographic-render-data new-perspective-render-data] (render-data/remove-temp-render-data state render-data-key)]
         (tab-pane/with-current-tab [tab _]
                                    (remove-render-data-from-tab tab orthographic-box perspective-box))
         (reset! state (assoc @state :orthographic-render-data new-orthographic-render-data
                                     :perspective-render-data new-perspective-render-data))))
   ;; perform picking for the new result
   (let [result (picking/pick-brushes (:world @state) ray-origin ray-direction)]
      (when (some? result)
         (let [current (picking/current-result result)
               entity-id (:entity-id current)
               brush-id (:brush-id current)
               brush (entity/get-brush (world/get-entity (:world @state) entity-id) brush-id)
               orthographic-box (renderer/brush->box brush)
               perspective-box (renderer/brush->box brush)
               render-data-key (str entity-id "-" brush-id)  ; TODO!!
               [new-orthographic-render-data new-perspective-render-data] (render-data/add-temp-render-data state render-data-key orthographic-box perspective-box)]
            (tab-pane/with-current-tab [tab _]
                                       (add-render-data-to-tab tab orthographic-box perspective-box))
            (reset! state (assoc @state :orthographic-render-data new-orthographic-render-data
                                        :perspective-render-data new-perspective-render-data))))
      (reset! state (assoc @state :picking-result result))))