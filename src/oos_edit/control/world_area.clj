;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.control.world-area
   (:require
      [oos-edit.control.tab-pane :as tab-pane]
      [oos-edit.control.content-bar :as content-bar]
      [oos-edit.control.orthographic-scene :as orthographic-scene]
      [oos-edit.control.perspective-scene :as perspective-scene]
      [oos-edit.control.scene-utils :as scene-utils]
      [oos-edit.keyboard :as keyboard]
      [oos-edit.view :as view]
      [oos-edit.picking :as picking]
      [oos-edit.brush :as brush]
      [oos-edit.entity :as entity]
      [oos-edit.world :as world])
   (:import
      (java.awt MouseInfo)
      (javafx.geometry Orientation Pos)
      (javafx.scene.control SplitPane)
      (javafx.scene.layout Priority StackPane VBox)))


;;;
;;; local-mouse-coordinates-from-tab
;;;

(defn- local-mouse-coordinates-from-tab
   [tab]
   (let [[scene _] (scene-utils/get-orthographic-scene-and-grid tab)
         location (.getLocation (MouseInfo/getPointerInfo))
         x (.getX location)
         y (.getY location)
         width (.getWidth scene)
         height (.getHeight scene)
         point (.screenToLocal scene x y)
         local-x (Math/round ^double (- (.getX point) (* width 0.5)))
         local-y (Math/round ^double (- (- (.getY point) (* height 0.5))))]
      [local-x, local-y]))


;;;
;;; handle-brush-function
;;;

(defn handle-brush-function
   [function]
   (tab-pane/with-current-tab [tab state]
                              (picking/with-selected-brush (:world @state) (:picking-result @state)
                                                           (fn [entity-id brush-id brush]
                                                              (when (= entity-id 0) ; only for the world entity
                                                                 (case function
                                                                    :to-additive (scene-utils/update-brush-in-state state entity-id brush-id (assoc brush :mode :additive))
                                                                    :to-subtractive (scene-utils/update-brush-in-state state entity-id brush-id (assoc brush :mode :subtractive))
                                                                    :to-first (println "to-first")
                                                                    :to-last (println "to-last")))))))


;;;
;;; handle-selection-keys
;;;

(defn- handle-selection-keys
   [keyboard-state state]
   (let [result (:picking-result @state)]
      (when (some? result)
         ;; clear selection
         (when (keyboard/is-key-press keyboard-state :clear-selection)
            (reset! state (assoc @state :picking-result nil)))
         ;; clone selection
         (when (keyboard/is-key-press keyboard-state :clone-selection)
            )
         ;; delete selection
         (when (keyboard/is-key-press keyboard-state :delete-selection)
            (let [result (picking/current-result result)]
               (scene-utils/remove-brush-from-state state (:entity-id result) (:brush-id result)))
            (reset! state (assoc @state :picking-result (picking/remove-result result)))))))


;;;
;;; handle-scene-material-keys
;;;

(defn- handle-scene-material-keys
   [state replace-side-material replace-brush-material fetch-material ray-origin ray-direction]
   (let [peeked (picking/peek-brush (:world @state) ray-origin ray-direction)]
      (when (some? peeked)
         (let [entity-id (:entity-id peeked)
               brush-id (:brush-id peeked)
               brush-side (:brush-side peeked)
               brush (entity/get-brush (world/get-entity (:world @state) entity-id) brush-id)]
            (when replace-side-material
               (scene-utils/update-brush-in-state state entity-id brush-id (brush/change-side-material brush brush-side (content-bar/selected-material))))
            (when replace-brush-material
               (scene-utils/update-brush-in-state state entity-id brush-id (brush/change-material brush (content-bar/selected-material))))
            (when fetch-material
               (content-bar/set-fetched-material (:material (nth (:sides brush) brush-side))))))))


;;;
;;; handle-orthographic-material-keys
;;;

(defn- handle-orthographic-material-keys
   [tab keyboard-state state]
   (let [replace-side-material (keyboard/is-key-press keyboard-state :replace-side-material)
         replace-brush-material (keyboard/is-key-press keyboard-state :replace-brush-material)
         fetch-material (keyboard/is-key-press keyboard-state :fetch-material)]
      (when (or replace-side-material replace-brush-material fetch-material)
         (let [view (orthographic-scene/current-orthographic-view state)
               [local-x local-y] (local-mouse-coordinates-from-tab tab)
               [view-x view-y] (view/transform-mouse view local-x local-y)
               [ray-origin ray-direction] (view/picking-ray view view-x view-y)]
            (handle-scene-material-keys state replace-side-material replace-brush-material fetch-material ray-origin ray-direction)))))


;;;
;;; handle-perspective-material-keys
;;;

(defn- handle-perspective-material-keys
   [scene keyboard-state state]
   (let [replace-side-material (keyboard/is-key-press keyboard-state :replace-side-material)
         replace-brush-material (keyboard/is-key-press keyboard-state :replace-brush-material)
         fetch-material (keyboard/is-key-press keyboard-state :fetch-material)]
      (when (or replace-side-material replace-brush-material fetch-material)
         (let [location (.getLocation (MouseInfo/getPointerInfo))
               local (.screenToLocal scene (.getX location) (.getY location))
               width (.getWidth scene)
               height (.getHeight scene)
               [mouse-x mouse-y] (perspective-scene/normalized-mouse-coordinates (.getX local) (.getY local) width height)
               [ray-origin ray-direction] (view/perspective-picking-ray (:perspective-view @state) mouse-x mouse-y width height)]
            (handle-scene-material-keys state replace-side-material replace-brush-material fetch-material ray-origin ray-direction)))))


;;;
;;; handle-material-keys
;;;

(defn- handle-material-keys
   [tab keyboard-state state]
   (let [[orthographic _] (scene-utils/get-orthographic-scene-and-grid tab)
         perspective (scene-utils/get-perspective-scene tab)]
      (when (scene-utils/mouse-inside-scene orthographic)
         (handle-orthographic-material-keys tab keyboard-state state))
      (when (scene-utils/mouse-inside-scene perspective)
         (handle-perspective-material-keys perspective keyboard-state state))))


;;;
;;; handle-keyboard
;;;

(defn handle-keyboard
   [keyboard-state delta-seconds]
   (tab-pane/with-current-tab [tab state]
                              (handle-selection-keys keyboard-state state)
                              (handle-material-keys tab keyboard-state state)
                              (orthographic-scene/handle-keyboard keyboard-state delta-seconds)
                              (perspective-scene/handle-keyboard keyboard-state delta-seconds)))


;;;
;;; create-stack-pane
;;;

(defn- create-stack-pane
   []
   (doto (StackPane.)
      (VBox/setVgrow Priority/ALWAYS)
      (StackPane/setAlignment Pos/CENTER)))


;;;
;;; create-world-area
;;;

(defn create-world-area
   [stage _ state]
   (let [left-box (VBox.)
         right-box (VBox.)
         split-pane (SplitPane.)
         orthographic-stack-pane (create-stack-pane)
         orthographic-grid (orthographic-scene/create-grid)
         orthographic-overlay (orthographic-scene/create-overlay)
         orthographic (orthographic-scene/create stage state)
         perspective-stack-pane (create-stack-pane)
         perspective-overlay (perspective-scene/create-overlay)
         perspective (perspective-scene/create stage state)]
      ;; orthographic
      (.setMinSize left-box 0.0 0.0)
      (.setFillWidth left-box true)
      (.setCamera orthographic-grid (view/create-orthographic-camera))
      (.bind (.widthProperty orthographic-grid) (.widthProperty left-box))
      (.bind (.heightProperty orthographic-grid) (.heightProperty left-box))
      (.bind (.widthProperty orthographic-overlay) (.widthProperty left-box))
      (.bind (.heightProperty orthographic-overlay) (.heightProperty left-box))
      (.bind (.widthProperty orthographic) (.widthProperty left-box))
      (.bind (.heightProperty orthographic) (.heightProperty left-box))
      (.add (.getChildren orthographic-stack-pane) orthographic)
      (.add (.getChildren orthographic-stack-pane) orthographic-grid)
      (.add (.getChildren orthographic-stack-pane) orthographic-overlay)
      (.add (.getChildren left-box) orthographic-stack-pane)
      ;; perspective
      (.setMinSize right-box 0.0 0.0)
      (.setFillWidth right-box true)
      (.bind (.widthProperty perspective-overlay) (.widthProperty right-box))
      (.bind (.heightProperty perspective-overlay) (.heightProperty right-box))
      (.bind (.widthProperty perspective) (.widthProperty right-box))
      (.bind (.heightProperty perspective) (.heightProperty right-box))
      (.add (.getChildren perspective-stack-pane) perspective)
      (.add (.getChildren perspective-stack-pane) perspective-overlay)
      (.add (.getChildren right-box) perspective-stack-pane)
      ;; split-pane
      (.setOrientation split-pane Orientation/HORIZONTAL)
      (.addAll (.getItems split-pane) [left-box right-box])
      split-pane))