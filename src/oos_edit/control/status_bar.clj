;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.control.status-bar
   (:import
      (javafx.scene.control Label)
      (javafx.scene.layout ColumnConstraints GridPane HBox)))


(def ^:dynamic *status-bar* (volatile! nil))


;;;
;;; create-section
;;;

(defn- create-section
   [pane text index percent]
   (let [label (Label. text)
         constraints (ColumnConstraints.)]
      (.setStyle label "-fx-padding: 5 0 0 8;")
      (GridPane/setConstraints label index 0)
      (.setPercentWidth constraints percent)
      (.add (.getChildren pane) label)
      (.add (.getColumnConstraints pane) constraints)))


;;;
;;; create-status-bar
;;;

(defn create-status-bar
   []
   (let [status-bar (HBox.)]
      (.setMinHeight status-bar 26.0)
      (.setPrefHeight status-bar 26.0)
      ;(.setStyle status-bar "-fx-background-color: rgb(85, 85, 85);")
      (let [pane (GridPane.)]
         (.bind (.prefWidthProperty pane) (.widthProperty status-bar))
         (create-section pane "" 0 20.0)
         (create-section pane "Editor ready." 1 55.0)
         (create-section pane "" 2 25.0)
         (.add (.getChildren status-bar) pane))
      (vreset! *status-bar* status-bar)
      status-bar))


;;;
;;; set-section-text
;;;

(defn set-section-text
   [section text]
   (let [pane (.get (.getChildren @*status-bar*) 0)]
      (.setText (.get (.getChildren pane)
                      (case section
                         :left 0
                         :center 1
                         :middle 1
                         :right 2)) text)))