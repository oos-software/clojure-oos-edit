;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.control.orthographic-scene
   (:require
      [oos-edit.keyboard :as keyboard]
      [oos-edit.mouse :as mouse]
      [oos-edit.view :as view]
      [oos-edit.position :as position]
      [oos-edit.math.vector :as vector]
      [oos-edit.geometry :as geometry]
      [oos-edit.construction :as construction]
      [oos-edit.picking :as picking]
      [oos-edit.control.tab-pane :as tab-pane]
      [oos-edit.control.status-bar :as status-bar]
      [oos-edit.control.scene-utils :as scene-utils]
      [oos-edit.menu.orthographic-menu :as orthographic-menu]
      [oos-edit.editor-misc :as editor-misc]
      [oos-edit.brush :as brush])
   (:import
      (javafx.scene SubScene SceneAntialiasing AmbientLight Group)
      (javafx.scene.paint Paint Color)
      (java.awt MouseInfo)
      (javafx.beans.value ChangeListener)
      (javafx.scene.shape Line Rectangle)
      (javafx.scene.text TextAlignment Font Text)))


(def ^:private ^:const +grid-size+ 128.0)
(def ^:private ^:const +default-material+ "engine/default.png") ; TODO: is this project specific?


;;;
;;; current-orthographic-view
;;;

(defn current-orthographic-view
   [state]
   (get @state (get @state :current-view)))


;;;
;;; update-orthographic-grid
;;;

(defn- update-orthographic-grid
   [state]
   (tab-pane/with-current-tab [tab _]
                              (let [[scene grid] (scene-utils/get-orthographic-scene-and-grid tab)
                                    camera (.getCamera grid)
                                    center-x (* (.getWidth scene) 0.5)
                                    center-y (* (.getHeight scene) 0.5)
                                    view (current-orthographic-view state)
                                    origin (:origin view)
                                    x (mod (vector/get-component origin 0) +grid-size+)
                                    y (- (mod (vector/get-component origin 2) +grid-size+))]
                                 (.setTranslateX camera (- x center-x))
                                 (.setTranslateY camera (- y center-y)))))


;;;
;;; update-orthographic-overlay
;;;

;(defn- update-orthographic-overlay
 ;  [state]



;;;
;;; update-orthographic-transformation
;;;

(defn- update-orthographic-transformation
   [scene state]
   (let [camera (.getCamera scene)
         transforms (.getTransforms camera)
         center-x (* (.getWidth scene) 0.5)
         center-y (* (.getHeight scene) 0.5)
         view (current-orthographic-view state)
         offset (view/offset center-x center-y)
         initial (view/initial-orthographic-rotation view)
         scale (view/initial-orthographic-scale view)
         trans (view/build-transformation (:origin view) (:rotation view))]
      (.clear transforms)
      (.add transforms initial)
      (.add transforms scale)
      (.addAll transforms trans)
      (.add transforms offset)))


;;;
;;; translate-orthographic-view
;;;

(defn- translate-orthographic-view
   [scene state move-x move-y]
   (let [view (current-orthographic-view state)
         new-view (view/translate view move-x move-y)]
      (reset! state (assoc @state (:current-view @state) new-view))
      (update-orthographic-transformation scene state)
      (update-orthographic-grid state)))


;;;
;;; build-construction-position
;;;

(defn- build-construction-position
   [view view-x view-y fixed-2]
   (let [[coord-0 coord-1] (view/used-coordinates view)
         coord-2 (view/unused-coordinate view)
         fixed-0 (position/integer->fixed view-x)
         fixed-1 (position/integer->fixed view-y)]
      (position/replace-component (position/replace-component (position/replace-component position/+zero+ coord-0 fixed-0) coord-1 fixed-1) coord-2 fixed-2)))


;;;
;;; local-mouse-coordinates-from-event
;;;

(defn- local-mouse-coordinates-from-event
   [event]
   (let [source (.getSource event)
         width (.getWidth source)
         height (.getHeight source)
         local-x (Math/round ^double (- (.getX event) (* width 0.5)))
         local-y (Math/round ^double (- (- (.getY event) (* height 0.5))))]
      [local-x local-y]))


;;;
;;; on-orthographic-scene-mouse-moved
;;;

(defn- on-orthographic-scene-mouse-moved
   [_ _ event state _]
   (.consume event)
   (let [view (current-orthographic-view state)
         [local-x local-y] (local-mouse-coordinates-from-event event)
         [view-x view-y] (view/transform-mouse view local-x local-y)]
      (status-bar/set-section-text :left (str (view/display-name view) ": " (long view-x) ", " (long view-y)))))


;;;
;;; on-orthographic-scene-mouse-pressed
;;;

(defn- on-orthographic-scene-mouse-pressed
   [_ _ event state mouse-state]
   (.consume event)
   (let [x (.getX event)
         y (.getY event)
         button (mouse/event-button-name event)
         dragged (mouse/event-dragged-name event)
         start-pos (mouse/event-pos-name event)]
      (when (= :left-button button)
         (let [view (current-orthographic-view state)
               [local-x local-y] (local-mouse-coordinates-from-event event)
               [view-x view-y] (view/transform-mouse view local-x local-y)]
            (if (.isShiftDown event)
               (let [[ray-origin ray-direction] (view/picking-ray view local-x local-y)]
                  (scene-utils/perform-picking state ray-origin ray-direction))
               (construction/begin (build-construction-position view view-x view-y 0) (editor-misc/get-geometry-snapping false)))))
      (vswap! mouse-state assoc button true dragged false start-pos [x y])))


;;;
;;; on-orthographic-scene-mouse-dragged
;;;

(defn- on-orthographic-scene-mouse-dragged
   [_ scene event state mouse-state]
   (.consume event)
   (let [x (.getX event)
         y (.getY event)
         ms @mouse-state]
      (when (:left-button ms)
         )                                                  ; TODO: move selection etc.
      (when (:right-button ms)
         (let [dragged (:right-dragged ms)
               [start-x start-y] (:right-pos ms)]
            (if dragged
               (let [move-x (* (- x start-x) 1)             ;+mouse-sensitivity+)
                     move-y (* (- y start-y) -1)]           ;+mouse-sensitivity+)]
                  (translate-orthographic-view scene state move-x move-y)
                  (vswap! mouse-state assoc :right-pos [x y]))
               (when (or (>= (Math/abs (double (- start-x x))) scene-utils/+mouse-drag-distance+)
                         (>= (Math/abs (double (- start-y y))) scene-utils/+mouse-drag-distance+))
                  (vswap! mouse-state assoc :right-dragged true :right-pos [x y])))))))


;;;
;;; on-orthographic-scene-mouse-released
;;;

(defn- on-orthographic-scene-mouse-released
   [stage _ event state mouse-state]
   (.consume event)
   (let [button (mouse/event-button-name event)]
      ;; left
      (when (and (= :left-button button) (construction/is-construction))
         (let [view (current-orthographic-view state)
               [local-x local-y] (local-mouse-coordinates-from-event event)
               [view-x view-y] (view/transform-mouse view local-x local-y)
               fixed-2 (position/integer->fixed 16)         ; TODO!!
               [start end] (construction/finish (build-construction-position view view-x view-y fixed-2) (editor-misc/get-geometry-snapping false))
               diff (position/abs (position/subtract end start))
               brush (geometry/box-brush start end +default-material+)]
            (when (and (> (:x diff) 0) (> (:y diff) 0) (> (:z diff) 0))
               (scene-utils/add-brush-to-state state 0 brush))))
      ;; right
      (when (and (= :right-button button) (= (:right-dragged @mouse-state) false))
         (let [menu (orthographic-menu/create-orthographic-menu #(scene-utils/add-entity-to-state state %))]
            (.setAutoHide menu true)
            (.show menu stage (.getScreenX event) (.getScreenY event))))
      (vswap! mouse-state assoc button false)))


;;;
;;; switch-orthographic-scene-view
;;;

(defn- switch-orthographic-scene-view
   [tab state]
   (let [[scene, _] (scene-utils/get-orthographic-scene-and-grid tab)
         view (current-orthographic-view state)
         width (.getWidth scene)
         height (.getHeight scene)]
      (update-orthographic-transformation scene state)
      (let [location (.getLocation (MouseInfo/getPointerInfo))
            x (.getX location)
            y (.getY location)
            point (.screenToLocal scene x y)
            local-x (Math/round ^double (- (.getX point) (* width 0.5)))
            local-y (Math/round ^double (- (- (.getY point) (* height 0.5))))
            [view-x view-y] (view/transform-mouse view local-x local-y)]
         (update-orthographic-grid state)
         (status-bar/set-section-text :left (str (view/display-name view) ": " (long view-x) ", " (long view-y))))))


;;;
;;; handle-view-switch
;;;

(defn- handle-view-switch
   [tab keyboard-state state]
   (let [set-current-view (fn [mode]
                             (reset! state (assoc @state :current-view mode)))]
      (doseq [view [:side-view :front-view :top-view]]
         (when (keyboard/is-key-press keyboard-state view)
            (set-current-view view)
            (switch-orthographic-scene-view tab state)))))


;;;
;;; handle-transformation-keys
;;;

(defn- handle-transformation-keys
   [keyboard-state state]
   (picking/with-selected-brush (:world @state) (:picking-result @state)
                                (fn [entity-id brush-id brush]
                                   (let [snapping (editor-misc/get-geometry-snapping true)
                                         view (current-orthographic-view state)
                                         [u-axis v-axis] (view/main-axes view)
                                         u-axis (vector/scale u-axis snapping)
                                         v-axis (vector/scale v-axis snapping)
                                         inv-u-axis (position/from-vector (vector/inverse u-axis))
                                         inv-v-axis (position/from-vector (vector/inverse v-axis))
                                         u-axis (position/from-vector u-axis)
                                         v-axis (position/from-vector v-axis)]
                                      (let [handle (fn [key axis-1 axis-2]
                                                      (when (keyboard/is-key-press keyboard-state key)
                                                         (let [[ctrl alt _] (keyboard/get-modifier-keys keyboard-state key)]
                                                            (if (or ctrl alt)
                                                               (let [index (brush/find-side brush axis-1)]
                                                                  (if ctrl
                                                                     (scene-utils/update-brush-in-state state entity-id brush-id (brush/translate-side brush index axis-1))
                                                                     (scene-utils/update-brush-in-state state entity-id brush-id (brush/translate-side brush index axis-2))))
                                                               (scene-utils/update-brush-in-state state entity-id brush-id (brush/translate brush axis-1))))))]
                                         (handle :left-arrow inv-u-axis u-axis)
                                         (handle :right-arrow u-axis inv-u-axis)
                                         (handle :down-arrow inv-v-axis v-axis)
                                         (handle :up-arrow v-axis inv-v-axis))))))


;;;
;;; handle-keyboard
;;;

(defn handle-keyboard
   [keyboard-state _]
   (tab-pane/with-current-tab [tab state]
                              (handle-view-switch tab keyboard-state state)
                              (handle-transformation-keys keyboard-state state)))


;;;
;;; create-grid
;;;

(defn create-grid
   []
   (let [light (AmbientLight.)
         group (Group. [light])
         resolution 8000.0
         start (- resolution)
         count (Math/ceil (/ (* resolution 2) +grid-size+))
         style "-fx-stroke: #222222;"]
      (loop [i 0]
         (when (< i count)
            (let [step (+ start (* i +grid-size+))]
               ;; horizontal
               (.add (.getChildren group)
                     (doto (Line. start step resolution step)
                        (.setStyle style)))
               ;; vertical
               (.add (.getChildren group)
                     (doto (Line. step start step resolution)
                        (.setStyle style))))
            (recur (inc i))))
      (SubScene. group 0.0 0.0 false SceneAntialiasing/DISABLED)))


;;;
;;; create-overlay
;;;

;(defn create-overlay
;   []
;   (let [light (AmbientLight.)
;         group (Group. [light])]
;      (SubScene. group 0.0 0.0 false SceneAntialiasing/DISABLED)))
(defn create-overlay
   []
   (let [light (AmbientLight.)
         group (Group. [light])
         rectangle (Rectangle. 300.0 40.0)
         text (Text. 100.0 100.0 "128x128 (1mx1m)")
         scene (SubScene. group 0.0 0.0 false SceneAntialiasing/DISABLED)]
      (.setTextAlignment text TextAlignment/CENTER)
      (.setFont text (Font. 20))
      (.setStroke text Color/FUCHSIA)
      (.setFill text Color/FUCHSIA)
      (.setFill rectangle Color/WHITE)
      (.setMouseTransparent text true)
      (.setTranslateX rectangle 100.0)
      (.setTranslateY rectangle 100.0)
      (.add (.getChildren group) rectangle)
      (.add (.getChildren group) text)
      scene))


;;;
;;; create
;;;

(defn create
   [stage state]
   (let [light (AmbientLight.)
         group (Group. [light])
         scene (SubScene. group 0.0 0.0 true SceneAntialiasing/DISABLED)
         listener (reify ChangeListener
                     (changed [_ _ _ _]
                        (update-orthographic-transformation scene state)
                        (update-orthographic-grid state)))]
      (.setFill scene (Paint/valueOf "#111111"))            ;"black"))
      (.setCamera scene (view/create-orthographic-camera))
      (.addListener (.widthProperty scene) listener)
      (.addListener (.heightProperty scene) listener)
      (let [mouse-state (volatile! (mouse/create-state))]
         (.setOnMouseMoved scene (scene-utils/event-handler stage scene state mouse-state on-orthographic-scene-mouse-moved))
         (.setOnMousePressed scene (scene-utils/event-handler stage scene state mouse-state on-orthographic-scene-mouse-pressed))
         (.setOnMouseDragged scene (scene-utils/event-handler stage scene state mouse-state on-orthographic-scene-mouse-dragged))
         (.setOnMouseReleased scene (scene-utils/event-handler stage scene state mouse-state on-orthographic-scene-mouse-released)))
      scene))