;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.control.tool-bar
   (:import
      (javafx.scene.control ToolBar Button Separator)
      (javafx.scene.image Image ImageView)
      (javafx.scene Node)
      (javafx.geometry Orientation)
      (javafx.event EventHandler)))


;(def ^:dynamic *main-tool-bar* (volatile! nil))
;(def ^:dynamic *left-tool-bar* (volatile! nil))


;;;
;;; create-tool-bar-button
;;;

(defn- create-tool-bar-button
   [^String file-name event-handler event]
   (let [image (Image. file-name)
         view (ImageView. image)
         button (Button.)]
      (.setGraphic button view)
      (.setOnAction button (reify EventHandler
                              (handle [_ _]
                                 (event-handler {:event event}))))
      button))


;;;
;;; create-tool-bar-buttons
;;;

(defn- create-tool-bar-buttons
   [event-handler & buttons]
   (map (fn [[file-name event]]
           (if (= file-name "")
              (Separator.)
              (create-tool-bar-button (str "editor/icons/" file-name) event-handler event))) buttons))


;;;
;;; create-main-tool-bar
;;;

(defn create-main-tool-bar
   [event-handler]
   (let [tool-bar (ToolBar.)]
      (.setMinHeight tool-bar 42.0)
      (.setPrefHeight tool-bar 42.0)
      (.add (.getStylesheets tool-bar) "editor/css/tool-bar.css")
      (let [buttons (create-tool-bar-buttons event-handler
                                             ["new.png" :world/new]
                                             ["open.png" :world/open]
                                             ["" :separator]
                                             ["save.png" :world/save]
                                             ["save-as.png" :world/save-as]
                                             ["" :separator]
                                             ["settings.png" :world/settings]
                                             ["" :separator]
                                             ["compile.png" :world/compile]
                                             ["" :separator]
                                             ["undo.png" :edit/undo]
                                             ["redo.png" :edit/redo])]
         (.addAll (.getItems tool-bar) (into-array Node buttons)))
      ;(vreset! *main-tool-bar* tool-bar)
      tool-bar))


;;;
;;; create-left-tool-bar
;;;

(defn create-left-tool-bar
   [event-handler]
   (let [tool-bar (ToolBar.)]
      (.setMinWidth tool-bar 42.0)
      (.setPrefWidth tool-bar 42.0)
      (.setOrientation tool-bar Orientation/VERTICAL)
      (.add (.getStylesheets tool-bar) "editor/css/tool-bar.css")
      (let [buttons (create-tool-bar-buttons event-handler
                                             ["curve.png" :tool/curve])]
         (.addAll (.getItems tool-bar) (into-array Node buttons)))
      ;(vreset! *left-tool-bar* tool-bar)
      tool-bar))