;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.entities
   (:require
      [oos-edit.entity :as entity]))


; TODO: this is project specific?


(def ^:private ^:dynamic *registered-entities* (volatile! {}))


(defn- register-entity
   [class-name spawner]
   (vreset! *registered-entities* (assoc @*registered-entities* class-name spawner)))


;;;
;;; player-start-component
;;;

(defn- player-start-component
   []
   [:player-start {}])


;;;
;;; bounding-component
;;;

(defn- bounding-component
   [min max]
   [:bounding {:min min
               :max max}])


;;;
;;; light-component
;;;

(defn- light-component
   [type is-static]
   [:light {:type type
            :offset "0.0,0.0,0.0"  ; offset from entity origin
            :color "1.0,1.0,1.0"
            :is-static is-static}])  ; only static lights will be used for light mapping


;;;
;;; spawn-info-player-start
;;;

(defn- spawn-info-player-start
   []
   [(player-start-component)
    (bounding-component "-20.0,-20.0,-60.0" "20.0,20.0,60.0")])


;;;
;;; spawn-light-sky
;;;

(defn- spawn-light-sky
   []
   [(light-component "sky" "true")])


;;;
;;; spawn-light-point
;;;

(defn- spawn-light-point
   []
   [(light-component "point" "true")])


;;;
;;; spawn
;;;

(defn spawn
   [class]
   (let [entity (entity/create class)
         spawner (get @*registered-entities* class)
         components (spawner)]
      (loop [i 0 result entity]
         (if (< i (count components))
            (let [[comp-class comp-data] (nth components i)]
               (recur (inc i)
                      (entity/add-component result comp-class comp-data)))
            result))))


;;;
;;; all registered entities
;;;

(register-entity :info-player-start spawn-info-player-start)

(register-entity :light-sky spawn-light-sky)
(register-entity :light-point spawn-light-point)