;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.geometry
   (:require
      [oos-edit.math.vector :as vector]
      [oos-edit.math.quaternion :as quaternion]
      [oos-edit.position :as position]
      [oos-edit.brush :as brush]))


(def ^:const +default-scale-u+ 0.25)
(def ^:const +default-scale-v+ -0.25)


;;;
;;; create-side
;;;

(defn- create-side
   [px py pz nx ny nz material]
   (let [offset (position/create px py pz)
         normal (vector/->Vector3 nx ny nz)]
      (brush/create-side offset normal 0.0 0.0 +default-scale-u+ +default-scale-v+ 0.0 :world material)))


;;;
;;; box-brush
;;;

(defn box-brush
   [min-pos max-pos ^String material]
   (let [origin (position/create
                   (quot (+ (:x min-pos) (:x max-pos)) 2)
                   (quot (+ (:y min-pos) (:y max-pos)) 2)
                   (quot (+ (:z min-pos) (:z max-pos)) 2))
         left (create-side (- (:x min-pos) (:x origin)) (:y origin) (:z origin) -1.0 0.0 0.0 material)
         right (create-side (- (:x max-pos) (:x origin)) (:y origin) (:z origin) 1.0 0.0 0.0 material)
         front (create-side (:x origin) (- (:y min-pos) (:y origin)) (:z origin) 0.0 -1.0 0.0 material)
         back (create-side (:x origin) (- (:y max-pos) (:y origin)) (:z origin) 0.0 1.0 0.0 material)
         bottom (create-side (:x origin) (:y origin) (- (:z min-pos) (:z origin)) 0.0 0.0 -1.0 material)
         top (create-side (:x origin) (:y origin) (- (:z max-pos) (:z origin)) 0.0 0.0 1.0 material)]
      (brush/create origin quaternion/+unit+ [left right front back bottom top] :additive)))


;;;
;;; stair-brush
;;;

(defn stair-brush
   []
   )