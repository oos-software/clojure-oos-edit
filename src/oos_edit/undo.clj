;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.undo)


(def ^:private ^:const +max-steps+ 150)


;;;
;;; create-step
;;;

(defn create-step
   [step-type content]
   {:type    step-type                                      ; :brush
    :content content})


;;;
;;; create-state
;;;

(defn create-state
   []
   {:position 0                                             ; this is always the end of the vector if a new step is added
    :steps    []})


;;;
;;; add-undo-step
;;;
;;; if we have done an undo before without a redo, the remaining redo-steps are discarded
;;;

(defn add-undo-step
   [state step-type content]
   (let [step (create-step step-type content)
         new-steps (subvec (:steps state) 0 (+ (:position state) 1))]
      (assoc state :position (- (count new-steps) 1) :steps (conj new-steps step))))


;;;
;;; perform-undo-step
;;;

(defn perform-undo-step
   [world state]
   )


;;;
;;; perform-redo-step
;;;

(defn perform-redo-step
   [state]
   )