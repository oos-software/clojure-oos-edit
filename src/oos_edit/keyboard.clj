;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.keyboard
   (:import
      (javafx.scene.input KeyCode)))


;;;
;;; create-modifier-state
;;;

(defn- create-modifier-state
   [event]
   {:ctrl  (.isControlDown event)
    :alt   (.isAltDown event)
    :shift (.isShiftDown event)})


;;;
;;; create-state
;;;

(defn create-state
   []
   {:side-view              false
    :front-view             false
    :top-view               false
    :move-left              false
    :move-right             false
    :move-backward          false
    :move-forward           false
    :move-down              false
    :move-up                false
    :speed                  false
    :clear-selection        false
    :clone-selection        false
    :delete-selection       false
    :left-arrow             false
    :right-arrow            false
    :down-arrow             false
    :up-arrow               false
    :rebuild-geometry       false
    :replace-side-material  false
    :replace-brush-material false
    :fetch-material         false
    :modifier-states        {}                              ; key->modifiers
    :key-consumed           #{}})


;;;
;;; event->key
;;;

(defn- event->key
   [event]
   (let [code (.getCode event)]
      (cond
         (= code KeyCode/DIGIT1) :side-view
         (= code KeyCode/DIGIT2) :front-view
         (= code KeyCode/DIGIT3) :top-view
         (= code KeyCode/A) :move-left
         (= code KeyCode/D) :move-right
         (= code KeyCode/S) :move-backward
         (= code KeyCode/W) :move-forward
         (= code KeyCode/SUBTRACT) :move-down
         (= code KeyCode/ADD) :move-up
         (= code KeyCode/SHIFT) :speed
         (= code KeyCode/ESCAPE) :clear-selection
         (= code KeyCode/SPACE) :clone-selection
         (= code KeyCode/DELETE) :delete-selection
         (= code KeyCode/LEFT) :left-arrow
         (= code KeyCode/RIGHT) :right-arrow
         (= code KeyCode/DOWN) :down-arrow
         (= code KeyCode/UP) :up-arrow
         (= code KeyCode/F5) :rebuild-geometry
         (= code KeyCode/E) :replace-side-material
         (= code KeyCode/R) :replace-brush-material
         (= code KeyCode/T) :fetch-material)))


;;;
;;; handle-key
;;;

(defn- handle-key
   [state event down]
   (let [key (event->key event)]
      (when (some? key)
         (.consume event)
         (let [key-consumed (get @state :key-consumed)
               modifier-states (get @state :modifier-states)]
            (let [new-key-consumed (if down
                                      key-consumed
                                      (disj key-consumed key))
                  new-modifier-states (if down
                                         (assoc modifier-states key (create-modifier-state event))
                                         (dissoc modifier-states key))]
               (vswap! state assoc key down :key-consumed new-key-consumed :modifier-states new-modifier-states))))))


;;;
;;; on-key-pressed
;;;

(defn on-key-pressed
   [state event]
   (handle-key state event true))


;;;
;;; on-key-released
;;;

(defn on-key-released
   [state event]
   (handle-key state event false))


;;;
;;; get-modifier-keys
;;;

(defn get-modifier-keys
   [state key]
   (let [modifiers (get (get @state :modifier-states) key)]
      [(:ctrl modifiers) (:alt modifiers) (:shift modifiers)]))


;;;
;;; is-key-press
;;;

(defn is-key-press
   [state key]
   (if (get @state key)
      (let [key-consumed (get @state :key-consumed)]
         (if (contains? key-consumed key)
            false
            (do (vswap! state assoc :key-consumed (conj key-consumed key))
                true)))
      false))