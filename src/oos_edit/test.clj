(ns oos-edit.test
   (:require
      [oos-edit.position :as position]
      [oos-edit.brush :as brush]
      [oos-edit.geometry :as geometry]
      [oos-edit.csg :as csg]))


(def test-brush (geometry/box-brush (position/from-integer -128 -128 -128) (position/from-integer 128 128 128) "engine/default.png"))
(def test-cutter (geometry/box-brush (position/from-integer -32 -128 -32) (position/from-integer 32 128 128) "engine/default.png"))


(def test-brush-planes (map brush/create-planes [test-brush]))
(def test-brush-windings (map brush/create-windings [test-brush]))

(def test-cutter-planes (brush/create-planes test-cutter))
(def test-cutter-windings (brush/create-windings test-cutter))


(def test-windings (nth oos-edit.test/test-brush-windings 0))
(def cutter-plane (nth oos-edit.test/test-cutter-planes 0))

;(oos-edit.csg/brush-overlapping oos-edit.test/test-cutter-planes (nth oos-edit.test/test-brush-windings 0))

;(def partitioned (csg/partition-windings cutter-plane (nth oos-edit.test/test-brush-windings 0)))


;(def test-vector (map vector (map #(csg/count-sides cutter-plane %) test-windings) test-windings))
;(def front-windings (filter (fn [[[num-front num-back _] _]] (and (> num-front 0) (= num-back 0))) test-vector))
;(def back-windings (filter (fn [[[num-front num-back _] _]] (and (= num-front 0) (> num-back 0))) test-vector))
;(def both-windings (filter (fn [[[num-front num-back _] _]] (and (> num-front 0) (> num-back 0))) test-vector))





(defn test-csg
   []
   (csg/perform-subtraction test-cutter [test-brush]))