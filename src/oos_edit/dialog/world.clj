;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.dialog.world
   (:import
      (javafx.scene.control Dialog ButtonType TextField)
      (javafx.scene.layout VBox)
      (javafx.geometry Insets)))


;;;
;;; show-new-dialog
;;;

(defn show-new-dialog
   []
   (let [dialog (Dialog.)
         pane (.getDialogPane dialog)
         box (VBox.)
         author (TextField. "")
         name (TextField. "")]
      (.setTitle dialog "New World")  ; TODO: translation
      (.setFillWidth box true)
      (.setMinWidth author 400.0)
      (.setPrefWidth author 400.0)
      (.setMaxWidth author 400.0)
      (.setMinWidth name 400.0)
      (.setPrefWidth name 400.0)
      (.setMaxWidth name 400.0)
      (VBox/setMargin author (Insets. 6 6 6 6))
      (VBox/setMargin name (Insets. 6 6 6 6))
      (.addAll (.getChildren box) (into-array [author name]))
      (.add (.getChildren pane) box)
      (.addAll (.getButtonTypes pane) (into-array [ButtonType/OK ButtonType/CANCEL]))
      (.showAndWait dialog)))


;;;
;;; show-settings-dialog
;;;

(defn show-settings-dialog
   []
   )