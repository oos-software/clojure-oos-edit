;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.entity
   (:require
      [oos-edit.math.vector :as vector]
      [oos-edit.math.quaternion :as quaternion]
      [oos-edit.position :as position]
      [oos-edit.brush :as brush]))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;
;;;; an entity is transformable inside the editor
;;;; multiple brushes can be grouped into the same entity
;;;;
;;;; the world is its own (usually large) entity with lots of brushes
;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;
;;; create
;;;

(defn create
   [class]
   {:class             class
    :origin            position/+zero+
    :scale             vector/+one+
    :orientation       quaternion/+unit+
    :next-min-brush-id (long -1)                            ; always decreasing, never reused
    :next-max-brush-id (long 0)                             ; always increasing, never reused
    :group             ""                                   ; grouped entities can be selected as one with a special key combination
    :components        (sorted-map)
    :brushes           (sorted-map)})


;;;
;;; def-entity-changer
;;;

(defmacro def-entity-changer
   [key]
   `(defn- ~(symbol (str "change-entity-" (name key)))
       [entity# value#]
       (assoc entity# ~key value#)))


(def-entity-changer :origin)
(def-entity-changer :scale)
(def-entity-changer :orientation)
(def-entity-changer :group)
(def-entity-changer :components)
(def-entity-changer :brushes)


;;;
;;; add-component
;;;

(defn add-component
   [entity ^:keyword class data]
   (change-entity-components entity (assoc (:components entity) class data)))


;;;
;;; remove-component
;;;

(defn remove-component
   [entity ^:keyword class]
   (change-entity-components entity (dissoc (:components entity) class)))


;;;
;;; get-brush
;;;

(defn get-brush
   [entity ^long id]
   (get (:brushes entity) id))


;;;
;;; add-brush
;;;

(defn add-brush
   [entity brush]
   (let [id (:next-max-brush-id entity)
         new-entity (assoc entity :next-max-brush-id (inc id))
         new-brushes (assoc (:brushes entity) id brush)]
      [id, (change-entity-brushes new-entity new-brushes)]))


;;;
;;; remove-brush
;;;

(defn remove-brush
   [entity ^long id]
   (let [new-brushes (dissoc (:brushes entity) id)]
      (change-entity-brushes entity new-brushes)))


;;;
;;; update-brush
;;;

(defn update-brush
   [entity ^long id brush]
   (let [new-brushes (assoc (:brushes entity) id brush)]
      (change-entity-brushes entity new-brushes)))


;;;
;;; move-brush
;;;

(defn- move-brush
   [entity ^long id variable update-fn]
   (let [new-id (get entity variable)
         new-entity (assoc entity variable (update-fn new-id))
         old-brush (get-brush entity id)
         new-brushes (assoc (dissoc (:brushes entity id)) new-id old-brush)]
      (change-entity-brushes new-entity new-brushes)))


;;;
;;; move-brush-to-first
;;;

(defn move-brush-to-first
   [entity ^long id]
   (move-brush entity id :next-min-brush-id dec))


;;;
;;; move-brush-to-last
;;;

(defn move-brush-to-last
   [entity ^long id]
   (move-brush entity id :next-max-brush-id inc))


;;;
;;; process-after-loading
;;;

(defn process-after-loading
   [entity]
   (assoc entity
      :components (into (sorted-map) (:components entity))
      :brushes (into (sorted-map) (:brushes entity))))