;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.math.quaternion
   (:require
      [oos-edit.math.vector :as vector]
      [oos-edit.math.utils :as utils]))


(defrecord Quaternion [^double x ^double y ^double z ^double w])


(def ^:const +unit+ (->Quaternion 0.0 0.0 0.0 1.0))


;;;
;;; from-point
;;;

(defn from-point
   [point]
   (->Quaternion (:x point) (:y point) (:z point) 0.0))


;;;
;;; from-axis-angle
;;;

(defn from-axis-angle
   [axis ^double angle]
   (let [r (utils/degrees->radians (* angle 0.5))
         s (Math/sin r)
         c (Math/cos r)]
      (let [x (* (:x axis) s)
            y (* (:y axis) s)
            z (* (:z axis) s)]
         (->Quaternion x y z c))))


;;;
;;; conjugate
;;;

(defn conjugate
   [quaternion]
   (let [x (- (:x quaternion))
         y (- (:y quaternion))
         z (- (:z quaternion))
         w (:w quaternion)]
      (->Quaternion x y z w)))


;;;
;;; normalize
;;;

(defn normalize
   [quaternion]
   (let [x (:x quaternion)
         y (:y quaternion)
         z (:z quaternion)
         w (:w quaternion)
         mag (+ (* x x) (* y y) (* z z) (* w w))]
      (if (< mag 0.000001)
         +unit+
         (->Quaternion (/ x mag) (/ y mag) (/ z mag) (/ w mag)))))


;;;
;;; multiply
;;;

(defn multiply
   [q1 q2]
   (let [x1 (:x q1) y1 (:y q1) z1 (:z q1) w1 (:w q1)
         x2 (:x q2) y2 (:y q2) z2 (:z q2) w2 (:w q2)]
      (let [x (- (+ (* w1 x2) (* x1 w2) (* y1 z2)) (* z1 y2))
            y (+ (- (* w1 y2) (* x1 z2)) (* y1 w2) (* z1 x2))
            z (+ (- (+ (* w1 z2) (* x1 y2)) (* y1 x2)) (* z1 w2))
            w (- (* w1 w2) (* x1 x2) (* y1 y2) (* z1 z2))]
         (->Quaternion x y z w))))


;;;
;;; rotate-vector
;;;

(defn rotate-vector
   [rotation vector]
   (let [result (multiply (multiply rotation (from-point vector)) (conjugate rotation))]
      (vector/->Vector3 (:x result) (:y result) (:z result))))