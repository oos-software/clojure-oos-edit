;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.math.plane
   (:require
      [oos-edit.math.vector :as vector]))


(defrecord Plane [normal ^double dist])


;;;
;;; from-origin-and-normal
;;;

(defn from-origin-and-normal
   [origin normal]
   (->Plane normal (vector/scalar-product normal origin)))


;;;
;;; from-points
;;;

(defn from-points
   [point-1 point-2 point-3]
   (let [dir-1 (vector/subtract point-3 point-2)
         dir-2 (vector/subtract point-1 point-2)
         normal (vector/normalize (vector/cross-product dir-1 dir-2))]
      (->Plane normal (vector/scalar-product normal point-2))))


;;;
;;; distance-from
;;;

(defn distance-from
   [plane point]
   (- (vector/scalar-product (:normal plane) point) (:dist plane)))


;;;
;;; inverse
;;;

(defn inverse
   [plane]
   (->Plane (vector/inverse (:normal plane)) (- (:dist plane))))


;;;
;;; intersect-ray
;;;

(defn intersect-ray
   [plane origin direction]
   (let [dist (distance-from plane origin)
         dot (vector/scalar-product (:normal plane) direction)]
      (if (or (and (> dist 0.0) (< dot -0.0001)) (and (< dist 0.0) (> dot 0.0001)))
         (/ dist (- dot))
         -1.0)))