;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.math.utils
   (:require
      [oos-edit.math.vector :as vector]))


;;;
;;; degrees->radians
;;;

(defn degrees->radians
   [^double deg]
   (* (/ deg 180.0) Math/PI))


;;;
;;; radians->degrees
;;;

(defn radians->degrees
   [^double rad]
   (* (/ rad Math/PI) 180.0))


;;;
;;; clamp
;;;

(defn clamp
   [value min-value max-value]
   (min (max value min-value) max-value))


;;;
;;; clamp-angle
;;;

(defn clamp-angle
   [angle]
   (let [angle-2 (loop [a angle]
                    (if (< a 0.0)
                       (recur (+ a 360.0))
                       a))]
      (loop [a angle-2]
         (if (>= a 360.0)
            (recur (- a 360.0))
            a))))


;;;
;;; closest-point-on-ray
;;;

(defn closest-point-on-ray
   [origin direction point]
   )


;;;
;;; closest-point-on-line-segment
;;;

(defn closest-point-on-line-segment
   [start end point]
   )