;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                      ;;;;
;;;;  Copyright (c) Holger Meisel. All rights reserved.                   ;;;;
;;;;                                                                      ;;;;
;;;;  This program and the accompanying materials are made                ;;;;
;;;;  available under the terms of the Eclipse Public License 2.0         ;;;;
;;;;  which is available at https://www.eclipse.org/legal/epl-2.0/        ;;;;
;;;;                                                                      ;;;;
;;;;  By using this software in any fashion, you are                      ;;;;
;;;;  agreeing to be bound by the terms of this license.                  ;;;;
;;;;  You must not remove this notice, or any other, from this software.  ;;;;
;;;;                                                                      ;;;;
;;;;  SPDX-License-Identifier: EPL-2.0                                    ;;;;
;;;;                                                                      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ns oos-edit.math.vector)


(defrecord Vector3 [^double x ^double y ^double z])


(def ^:const +zero+ (->Vector3 0.0 0.0 0.0))
(def ^:const +half+ (->Vector3 0.5 0.5 0.5))
(def ^:const +one+ (->Vector3 1.0 1.0 1.0))


;;;
;;; get-component
;;;

(defn get-component
   [vec index]
   (case index
      0 (:x vec)
      1 (:y vec)
      2 (:z vec)))


;;;
;;; replace-component
;;;

(defn replace-component
   [vec index ^double value]
   (case index
      0 (->Vector3 value (:y vec) (:z vec))
      1 (->Vector3 (:x vec) value (:z vec))
      2 (->Vector3 (:x vec) (:y vec) value)))


;;;
;;; add
;;;

(defn add
   [vec-1 vec-2]
   (->Vector3 (+ (:x vec-1) (:x vec-2))
              (+ (:y vec-1) (:y vec-2))
              (+ (:z vec-1) (:z vec-2))))


;;;
;;; subtract
;;;

(defn subtract
   [vec-1 vec-2]
   (->Vector3 (- (:x vec-1) (:x vec-2))
              (- (:y vec-1) (:y vec-2))
              (- (:z vec-1) (:z vec-2))))


;;;
;;; multiply
;;;

(defn multiply
   [vec-1 vec-2]
   (->Vector3 (* (:x vec-1) (:x vec-2))
              (* (:y vec-1) (:y vec-2))
              (* (:z vec-1) (:z vec-2))))


;;;
;;; scale
;;;

(defn scale
   [vec ^double s]
   (->Vector3 (* (:x vec) s)
              (* (:y vec) s)
              (* (:z vec) s)))


;;;
;;; div
;;;

(defn div
   [vec ^double s]
   (scale vec (/ 1.0 s)))


;;;
;;; round
;;;

(defn round
   [vec]
   (let [x (Math/round ^double (:x vec))
         y (Math/round ^double (:y vec))
         z (Math/round ^double (:z vec))]
      (->Vector3 x y z)))


;;;
;;; snap
;;;

(defn snap
   [vec ^double grid]
   (let [round (fn [x] (* (Math/round ^double (/ x grid)) grid))]
      (->Vector3 (round (:x vec)) (round (:y vec)) (round (:z vec)))))


;;;
;;; absolute
;;;

(defn absolute
   [vec]
   (let [x (Math/abs ^double (:x vec))
         y (Math/abs ^double (:y vec))
         z (Math/abs ^double (:z vec))]
      (->Vector3 x y z)))


;;;
;;; inverse
;;;

(defn inverse
   [vec]
   (->Vector3 (- (:x vec)) (- (:y vec)) (- (:z vec))))


;;;
;;; smallest-index
;;;

(defn smallest-index
   [vec]
   (let [a (absolute vec)
         x (:x a)
         y (:y a)
         z (:z a)]
      (if (and (<= x y) (<= x z))
         0
         (if (and (<= y x) (<= y z))
            1
            2))))


;;;
;;; largest-index
;;;

(defn largest-index
   [vec]
   (let [a (absolute vec)]
      (let [x (:x a)
            y (:y a)
            z (:z a)]
         (if (and (>= x y) (>= x z))
            0
            (if (and (>= y x) (>= y z))
               1
               2)))))


;;;
;;; scalar-product
;;;

(defn scalar-product
   [vec-1 vec-2]
   (+ (* (:x vec-1) (:x vec-2))
      (* (:y vec-1) (:y vec-2))
      (* (:z vec-1) (:z vec-2))))


;;;
;;; cross-product
;;;

(defn cross-product
   [vec-1 vec-2]
   (let [x (- (* (:y vec-1) (:z vec-2)) (* (:z vec-1) (:y vec-2)))
         y (- (* (:z vec-1) (:x vec-2)) (* (:x vec-1) (:z vec-2)))
         z (- (* (:x vec-1) (:y vec-2)) (* (:y vec-1) (:x vec-2)))]
      (->Vector3 x y z)))


;;;
;;; squared-magnitude
;;;

(defn squared-magnitude
   [vec]
   (scalar-product vec vec))


;;;
;;; magnitude
;;;

(defn magnitude
   [vec]
   (Math/sqrt (squared-magnitude vec)))


;;;
;;; normalize
;;;

(defn normalize
   [vec]
   (let [mag (magnitude vec)]
      (if (< mag 0.000001)
         +zero+
         (div vec mag))))


;;;
;;; perpendicular
;;;

(defn perpendicular
   [vec]
   (let [other (replace-component +zero+ (smallest-index vec) 1.0)]
      (normalize (cross-product vec other))))