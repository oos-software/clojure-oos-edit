(defproject oos-edit "0.0.5-SNAPSHOT"
  :description "brush based level editor"
  :url "https://gitlab.com/oos-software/clojure-oos-edit"
  :license {:name "EPL-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.1"],
                 [org.openjfx/javafx-base "18"]
                 [org.openjfx/javafx-controls "18"]
                 [org.openjfx/javafx-graphics "18"]
                 [org.openjfx/javafx-fxml "18"]
                 [org.openjfx/javafx-swing "18"]]
  :repl-options {:init-ns oos-edit.core})
